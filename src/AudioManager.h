#ifndef AUDIO_MGR_H_
#define AUDIO_MGR_H_

#include <iostream>
#include <vector>
using std::cout; using std::endl;
using std::string;

#include "al.h"
#include "alc.h"
#include "efx.h"
#include "EFX-Util.h"
#include "efx-creative.h"

#include <CoreStructures\CoreStructures.h>

using CoreStructures::GUClock;
using CoreStructures::GUQuaternion;
using CoreStructures::GUMatrix4;
using CoreStructures::GUVector4;


class AudioClip;

class AudioManager
{

private:

	static AudioManager* c_audioManager;
	AudioManager(); // private constructor

	ALCdevice* alcDevice;
	ALCcontext* alcContext;

	//ALuint source1;

	// The libary of audio clips available to the game(objects). One audio clip obj per .wav file
	std::vector<AudioClip*> audioClips;

	int searchClips(string name);

	// Unlike resourceMgr, this is private! Accessed via create source method.
	ALuint* getClip(string name);

public:

	void loadClip(string name, int frequency);

	~AudioManager();

	static AudioManager* getInstance();

	void init();

	void update(GUVector4* listenerPos, GUQuaternion* listenerOri, GUVector4* listenerVelocity);

	void test();

	void playSound(ALuint source);

	ALuint* createSource(string name);

	void updateSource(ALuint source, GUVector4* pos, GUVector4* vel, GUQuaternion* ori);


	// stole this from a forum, but replaced fopen
	// https://stackoverflow.com/a/16449838/13324011
	long get_file_size(char* filename)
	{
		FILE* fp;
		long n;

		/* Open file */
		errno_t err = fopen_s(&fp, filename, "r");

		/* Ignoring error checks */

		/* Find end of file */
		fseek(fp, 0L, SEEK_END);

		/* Get current position */
		n = ftell(fp);

		/* Close file */
		fclose(fp);

		/* Return the file size*/
		return n;
	}
};

#endif

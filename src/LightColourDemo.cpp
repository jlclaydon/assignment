#include "LightColourDemo.h"

#include "AudioManager.h"
#include "CameraController.h"
#include "InputManager.h"
#include "GameObject.h"
#include "Player.h"
#include "SkyBox.h"
#include "Tank.h"
#include "TimeUtil.h"

#include "SpotlightShader.h"

void LightColourDemo::setupAssets()
{
	GameObject* turntable = new GameObject("disc", "granite.jpg",
		GUVector4(0, -0.10, 0), GUAxisAngle(0, 0, 0, 0), GUVector4(1, 1, 1), &standardShader);
	
	gameObjects.push_back(turntable);
}

void LightColourDemo::init()
{
	Game::init();

	skybox = new SkyBox("resources\\textures\\skybox\\cloud\\", Game::cloudBox);

	setupPlayer("camo.png");
	playerGO->setPosition(GUVector4(0, 0, 0));

	shader = new SpotlightShader();
	shader->init();



	setupAssets();

	lightCol = GUVector4(0, 0, 0);
	lightPos = GUVector4(10, 5, 1);
	att = GUVector4(0.1,0.001,0.001);
}

void LightColourDemo::gameLoop()
{
	while (!quit)
	{
		update();
		display();
	}
}


void LightColourDemo::freeFunction(int id)
{
	switch (id)
	{
	case 7: 
		adjustRed(1);
		break;
	case 1:
		adjustRed(-1);
		break;
	case 8:
		adjustGreen(1);
		break;
	case 2:
		adjustGreen(-1);
		break;
	case 9:
		adjustBlue(1);
		break;
	case 3:
		adjustBlue(-1);

	}
}

void LightColourDemo::update()
{
	Game::update();

	
}

void LightColourDemo::display()
{
	GUMatrix4 t2 = mainCamera->projectionTransform() * camCon->getOrientation(); // Get camera orientation and view matrix
	GUMatrix4 rot = GUMatrix4(GUQuaternion(GUAxisAngle(1, 0, 0, PI))); // A rotation matrix, for flipping 180 degrees about the X
	t2 *= rot; // apply rotation to t2

	//4
	skybox->render(t2);

	GUMatrix4 T = getCameraMatrix();

	// Render all GameObjects
	for (int i = 0; i < gameObjects.size(); ++i)
	{
		shader->update(gameObjects[i]->getMVP(T), lightPos, lightCol, att, gameObjects[i]->getLightBoost());
		gameObjects[i]->render(T);
	}


	SDL_GL_SwapWindow(sdl_window);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	return;
}

void LightColourDemo::adjustRed(float input)
{
	lightCol.x += input * dT;

	if (lightCol.x < 0)
		lightCol.x = 0;

	if (lightCol.x > 1)
		lightCol.x = 1;

	printCols();
}

void LightColourDemo::adjustGreen(float input)
{
	lightCol.y += input * dT;

	if (lightCol.y < 0)
		lightCol.y = 0;

	if (lightCol.y > 1)
		lightCol.y = 1;

	printCols();
}

void LightColourDemo::adjustBlue(float input)
{
	lightCol.z += input * dT;

	if (lightCol.z < 0)
		lightCol.z = 0;

	if (lightCol.z > 1)
		lightCol.z = 1;

	printCols();
}

void LightColourDemo::printCols()
{
	int r = lightCol.x * 255;
	int g = lightCol.y * 255;
	int b = lightCol.z * 255;

	cout << "R: " << r << " G: " << g << " B: " << b << "\n";
}

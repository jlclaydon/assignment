#ifndef GAME_OBJECT_MESH_H_
#define GAME_OBJECT_MESH_H_

#include "aiWrapper.h"
#include "MeshVBO.h"

using std::string;

class GOmeshComponent
{
protected:

	int meshCount;
	MeshVBO** meshArray;

	string meshName;
	GLuint texture;
	string texNameAndExt;

public:
	
	GOmeshComponent();
	~GOmeshComponent();


	void init(const aiScene* scene, string meshName, string texNameAndExt);

	GLuint* getTexture();

	void render();

	string getName();
	string getTextureName();

};

#endif
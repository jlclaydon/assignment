#ifndef SKYBOX_H_
#define SKYBOX_H_

#include <glew\glew.h>
#include <CoreStructures\GUObject.h>


#include <iostream>
#include <vector>

using std::string;
using std::cout; using std::endl;
using std::vector;

#include <CoreStructures/GUMatrix4.h>
using CoreStructures::GUMatrix4;

class SkyBox : public CoreStructures::GUObject
{
public:

	SkyBox(string path, vector<string> filenames);

	void render(GUMatrix4 view);

private:
	GLuint	vertexArrayObj;
	GLuint vertexBuffer;
	GLuint shader;
		
	GLuint v;

	
	GLuint cubemapTex;

	void generateTextureCube(string dir, vector<string> filenames);
};

#endif


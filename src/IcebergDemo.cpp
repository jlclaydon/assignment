#include "IcebergDemo.h"

#include "AudioManager.h"
#include "CameraController.h"
#include "InputManager.h"
#include "GameObject.h"
#include "Player.h"
#include "SkyBox.h"
#include "Tank.h"
#include "TimeUtil.h"

vector<string> arcticSkybox
{
	"right.jpg",
	"left.jpg",
	"top.jpg",
	"bottom.jpg",
	"front.jpg",
	"back.jpg"
};

void IcebergDemo::setupAssets()
{
	setupPlayer("snow camo.jpg");
	playerGO->setPosition(GUVector4(3, 2.4, -5));
	playerGO->setOrientation(GUQuaternion(GUAxisAngle(0, 1, 0, PI)));
	
	gameObjects.push_back(new GameObject("boat", "old wood.jpg",
		GUVector4(-10, 1, 10), GUAxisAngle(0, 0, 0, 0), GUVector4(0.1, 0.1, 0.1), &standardShader));

	gameObjects.push_back(new GameObject("boat", "old wood.jpg",
		GUVector4(-12, 1, 14), GUAxisAngle(0, 1, 0, PI/4), GUVector4(0.1, 0.1, 0.1), &standardShader));

	gameObjects.push_back(new GameObject("boat", "old wood.jpg",
		GUVector4(8, 1, 14), GUAxisAngle(0, 1, 0, -PI / 4), GUVector4(0.1, 0.1, 0.1), &standardShader));


	gameObjects.push_back(new GameObject("iceberg", "snow.jpg",
		GUVector4(0, 1, 0), GUAxisAngle(0, 0, 0, 0), GUVector4(1, 1, 1), &standardShader));

	GameObject* iceSheet = new GameObject("floorplane", "ice.jpg",
		GUVector4(0, 1, 0), GUAxisAngle(0, 0, 0, 0), GUVector4(100, 0.01, 100), &standardShader);
			
	iceSheet->setAlpha(0.8);
	iceSheet->setTransparency(true);
	gameObjects.push_back(iceSheet);

	gameObjects.push_back(new GameObject("beast", "beast_texture.bmp",
		GUVector4(-12, 7, -30), GUAxisAngle(0, 1, 0, 0), GUVector4(4, 4, 4), &standardShader));

	GameObject* ghostBeast1 = new GameObject("beast", "beast_texture.bmp",
		GUVector4(-4, 7, -30), GUAxisAngle(0, 1, 0, 0), GUVector4(4, 4, 4), &standardShader);
	ghostBeast1->setAlpha(0.75);
	ghostBeast1->setTransparency(true);
	gameObjects.push_back(ghostBeast1);

	GameObject* ghostBeast2 = new GameObject("beast", "beast_texture.bmp",
		GUVector4(4, 7, -30), GUAxisAngle(0, 1, 0, 0), GUVector4(4, 4, 4), &standardShader);
	ghostBeast2->setAlpha(0.5);
	ghostBeast2->setTransparency(true);
	gameObjects.push_back(ghostBeast2);

	GameObject* ghostBeast3 = new GameObject("beast", "beast_texture.bmp",
		GUVector4(12, 7, -30), GUAxisAngle(0, 1, 0,0 ), GUVector4(4, 4, 4), &standardShader);
	ghostBeast3->setAlpha(0.25);
	ghostBeast3->setTransparency(true);
	gameObjects.push_back(ghostBeast3);



	
	lightCol.x = lightCol.y = 0.8; 
	lightCol.z = 1;
}

void IcebergDemo::init()
{
	Game::init();

	skybox = new SkyBox("resources\\textures\\skybox\\arctic\\", arcticSkybox);

	setupAssets();

	theSun = GUVector4(1, 0.85, 1);
}

void IcebergDemo::gameLoop()
{
	while (!quit)
	{
		update();
		display();
	}
}

void IcebergDemo::freeFunction(int id)
{
	switch (id)
	{

	}
}

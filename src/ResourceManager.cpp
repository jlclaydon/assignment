#include "ResourceManager.h"

ResourceManager* ResourceManager::c_resourceManager = NULL;

ResourceManager::ResourceManager()
{
}

ResourceManager::~ResourceManager()
{
	for (int i = 0; i < meshes.size(); ++i)
	{
		delete meshes[i];
		meshes[i] = nullptr;
	}
	
}

void ResourceManager::importModel(string name)
{
}

int ResourceManager::searchMeshes(string name, string textureNameExt)
{
	for (int i = 0; i < meshes.size(); ++i)
	{
		if (strcmp(name.c_str(), meshes[i]->getName().c_str()) == 0)
		{
			if (strcmp(meshes[i]->getTextureName().c_str(), textureNameExt.c_str()) == 0)
			{
				return i;
			}
		}
	}

	return -1;
}

GOmeshComponent* ResourceManager::createMeshComponent(string name, string texNameAndExt)
{
	string path = "resources\\3D_models\\";
	path += name;
	path += ".obj";

	const aiScene* model = aiImportModel(string(path),
		aiProcess_GenNormals |
		aiProcess_CalcTangentSpace |
		aiProcess_Triangulate |
		aiProcess_JoinIdenticalVertices |
		aiProcess_SortByPType);

	GOmeshComponent* meshComp = new GOmeshComponent();
	meshComp->init(model, name, texNameAndExt);



	return meshComp;
}


GOmeshComponent* ResourceManager::getOrCreate(string name, string texNameAndExt)
{
	int index = searchMeshes(name, texNameAndExt);

	if (index != -1)
	{
		cout << name << " found at " << index << "!" << endl;
		return meshes[index];
	}
	else
	{
		cout << name << " was not found, creating now!" << endl;
	}

	GOmeshComponent* meshComp = createMeshComponent(name, texNameAndExt);

	meshes.push_back(meshComp);

	return meshComp;
}

ResourceManager* ResourceManager::getInstance()
{
	if (c_resourceManager == NULL)
	{
		cout << "creating resource manager..." << endl;
		c_resourceManager = new ResourceManager();
	}
	return c_resourceManager;
}

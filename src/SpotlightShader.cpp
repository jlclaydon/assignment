#include "SpotlightShader.h"

#include "shader_setup.h"

void SpotlightShader::render()
{
}

void SpotlightShader::update(GUMatrix4 mvp, GUVector4 pos, GUVector4 col, GUVector4 att, int lightBoost)
{
	glUseProgram(shaderProgram);
	glUniformMatrix4fv(mvpLoc, 1, GL_FALSE, (const GLfloat*)&(mvp.M));
	glUniform3f(lightPos, pos.x, pos.y, pos.z);
	glUniform3f(lightCol, col.x, col.y, col.z);
	glUniform3f(lightAtt, att.x, att.y, att.z);
	glUniform1f(factor, lightBoost);

}

void SpotlightShader::init()
{
	shaderProgram = setupShaders(string("resources\\shaders\\spotlight.vshade"),
		string("resources\\shaders\\standard.fshade"));

	mvpLoc = glGetUniformLocation(shaderProgram, "mvpMatrix");
	lightPos = glGetUniformLocation(shaderProgram, "lightPos");
	lightCol = glGetUniformLocation(shaderProgram, "lightCol");
	lightAtt = glGetUniformLocation(shaderProgram, "lightAtt");
	factor = glGetUniformLocation(shaderProgram, "factor");
}


#ifndef MESH_VBO_H_
#define MESH_VBO_H_

#include "aiWrapper.h"

class MeshVBO
{
protected:
	GLuint VAO;

	// VBOs
	GLuint vertexPos;
	GLuint texCoords;
	GLuint faceNormals;
	GLuint faceIndicies;

	int faceCount;

public:
	MeshVBO();
	~MeshVBO();

	void init(const aiScene* scene, int index); 

	void render();

};

#endif

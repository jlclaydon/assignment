#ifndef CAM_CON_H_
#define CAM_CON_H_

#include <CoreStructures\CoreStructures.h>
#include <iostream>
using std::cout; using std::endl;

using CoreStructures::GUAxisAngle;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;


class CameraController
{
private:

	float dT; // deltaTime

//	GUVector4* playerPos;
//	GUQuaternion* playerOri;

	float xRot;
	float xRotSpeed;
	float xRotMin;
	float xRotMax;
	float yRot;
	float yRotSpeed;


	float yPosition;
	float yPositionSpeed;
	float zPosition;
	float zPositionSpeed;
	float zPosMin;

	GUQuaternion cameraOri;
	GUVector4 cameraPos;
	

public:

	CameraController();
	//void init(GUVector4* playerPos, GUQuaternion* playerOri);

	void update(float dT, GUVector4* playerPos, GUQuaternion* playerOri);

	GUQuaternion getOrientation();

	GUVector4 getPosition();

	void cameraRotX(float input);
	void cameraRotY(float input);

	
	void cameraPosZ(float input);

	void resetCam();

	
};

#endif


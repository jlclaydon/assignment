#include "MultipleLightSourceShader.h"

#include "shader_setup.h"

void MultipleLightSourceShader::render()
{
}

void MultipleLightSourceShader::update(GUMatrix4 mvp, GUMatrix4 lt, GUMatrix4 lx, GUMatrix4 ly, GUMatrix4 lz, GUMatrix4 lr, GUMatrix4 lg, GUMatrix4 lb, GUMatrix4 l1, GUMatrix4 l2, GUMatrix4 l3, float factor)
{
	glUseProgram(shaderProgram);
	glUniformMatrix4fv(mvpLoc, 1, GL_FALSE, (const GLfloat*)&(mvp.M));
	glUniformMatrix4fv(lightMatrix, 1, GL_FALSE, (const GLfloat*)&(lt.M));
	glUniformMatrix4fv(lightPosX, 1, GL_FALSE, (const GLfloat*)&(lx.M));
	glUniformMatrix4fv(lightPosY, 1, GL_FALSE, (const GLfloat*)&(ly.M));
	glUniformMatrix4fv(lightPosZ, 1, GL_FALSE, (const GLfloat*)&(lz.M));
	glUniformMatrix4fv(lightColR, 1, GL_FALSE, (const GLfloat*)&(lr.M));
	glUniformMatrix4fv(lightColG, 1, GL_FALSE, (const GLfloat*)&(lg.M));
	glUniformMatrix4fv(lightColB, 1, GL_FALSE, (const GLfloat*)&(lb.M));
	glUniformMatrix4fv(lightAttI, 1, GL_FALSE, (const GLfloat*)&(l1.M));
	glUniformMatrix4fv(lightAttII, 1, GL_FALSE, (const GLfloat*)&(l2.M));
	glUniformMatrix4fv(lightAttIII, 1, GL_FALSE, (const GLfloat*)&(l3.M));
	glUniform1f(_factor, factor);
}

void MultipleLightSourceShader::init()
{
	shaderProgram = setupShaders(string("resources\\shaders\\multi_src.vshade"),
		string("resources\\shaders\\standard.fshade"));

	mvpLoc = glGetUniformLocation(shaderProgram, "mvpMatrix");
	lightMatrix = glGetUniformLocation(shaderProgram, "lightMatrix");

	lightPosX = glGetUniformLocation(shaderProgram, "lx");
	lightPosY = glGetUniformLocation(shaderProgram, "ly");
	lightPosZ = glGetUniformLocation(shaderProgram, "lz");
	lightColR = glGetUniformLocation(shaderProgram, "lr");
	lightColG = glGetUniformLocation(shaderProgram, "lg");
	lightColB = glGetUniformLocation(shaderProgram, "lb");
	lightAttI = glGetUniformLocation(shaderProgram, "l1");;
	lightAttII = glGetUniformLocation(shaderProgram, "l2");
	lightAttIII = glGetUniformLocation(shaderProgram, "l3");
	_factor = glGetUniformLocation(shaderProgram, "factor");


	cout << lightAttI << endl;
	cout << lightColB << endl;
	cout << lightAttII << endl;
	cout << lightAttIII << endl;


}

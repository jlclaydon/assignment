#ifndef AUDIO_SOURCE_H_
#define AUDIO_SOURCE_H_

#define LOOP 0
#define PITCH 1
#define GAIN 2

#include <string>
using std::string;

#include "al.h"
#include <CoreStructures\CoreStructures.h>

using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;

class AudioSource
{
public:

	void play();

	AudioSource(string name);

	void update(GUVector4* position, GUVector4* velocity, GUQuaternion *orientation);

	void setLoop(float loop);
	void setPitch(float pitch);
	void setGain(float gain);
	

	void setProperty(int id, float val);
	
	/* property id - name 
	*	0 = loop
	*	1 = pitch
	*	2 = gain
	*
	*/

private:
	
	ALuint alSource;
};

#endif // !AUDIO_SOURCE_H_

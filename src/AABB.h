#ifndef AABB_H_
#define AABB_H_

#include <CoreStructures\GUObject.h>
#include <CoreStructures\GUMatrix4.h>
#include <glew\glew.h>

#include <iostream>
using std::string;
using std::cout; using std::endl;

class AABB : public CoreStructures::GUObject
{
private:

	GLuint					vertexArrayObj;

	GLuint					vertexBuffer;
	GLuint					colourBuffer;

	GLuint					indexBuffer;

	GLuint					shader;


public:

	AABB(float length, float width, float height);
	AABB();
	~AABB();

	void render(const CoreStructures::GUMatrix4& T);

	void printAabbCoords();
};

#endif
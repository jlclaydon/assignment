#include "Tank.h"

#include "ActionAudio.h"
#include "GameObject.h"
#include "GOaudioComponent.h"

int boostLow[] = {2, 7, 22, 32};
int boostHi[] = {4, 14, 27, 39};
int gearMax[] = {7, 20, 34, 52};
int gearMin[] = {1, 6, 16, 28};



float Tank::mapFunc(float x0, float x1, float y0, float y1, float v)
{
	return y0 + (y1 - y0) * ((v - x0) / (x1 - x0));
}

void Tank::setAccel(float input)
{
	input_accel = input;
}

void Tank::setTurn(float input)
{
	input_turn = input;
}

void Tank::setBrakes(bool brakesOn)
{
	this->brakesOn = brakesOn;
}

void Tank::setCruiseControl(float speed)
{
	cruiseControl = speed;
	cruiseControlActive = true;
}

void Tank::cancelCruiseControl()
{
	cruiseControlActive = false;
}

void Tank::init(GameObject* tankBody, GameObject* tankTurret)
{
	body = tankBody;
	turret = tankTurret;

	turretAngle = 0;

	// Setup the audio components 
	tankBody->getAudioComponent()->addActionAudio("heavy_brake_squeal", "brakes");
	tankBody->getAudioComponent()->addActionAudio("heavy_diesel", "engine");
	tankBody->getAudioComponent()->addActionAudio("whistle", "turbo");
	tankBody->getAudioComponent()->addActionAudio("tracks", "tracks");	
	
	// get pointers to audio sources
    aaBrakes = tankBody->getAudioComponent()->getActionAudio("brakes");
    aaEngine = tankBody->getAudioComponent()->getActionAudio("engine");
	aaTracks = tankBody->getAudioComponent()->getActionAudio("tracks");
    aaTurbo = tankBody->getAudioComponent()->getActionAudio("turbo");
	
	aaBrakes->setProperty(LOOP, 1);
	aaEngine->setProperty(LOOP, 1);
	aaTracks->setProperty(LOOP, 1);	
	aaTurbo->setProperty(LOOP, 1);

	// Make these all silent else very loud first frame!
	aaBrakes->setProperty(GAIN, 0);
	aaEngine->setProperty(GAIN, 0);
	aaTracks->setProperty(GAIN, 0);
	aaTurbo->setProperty(GAIN, 0);

	// Begin play. The update method handles things from here.
	aaBrakes->play();
	aaEngine->play();
	aaTracks->play();
	aaTurbo->play();

	forwardVelocity = 0;
}


void Tank::turn(float angle)
{
	// This is a little naive. Im turning the vehicle by some arbitary amount, the only control being 
	// the input (and delta time), and rotating the velocity vector by the same degree. Not how it works in reality 
	// of course, but will do for now! 
	float turn = dT * angle;

	
	GUQuaternion rotation = GUQuaternion(GUAxisAngle(0, 1, 0, turn));

	body->setOrientation(body->getOrientation() * rotation);

	GUVector4 velocity = body->getVelocity();
	rotation.rotateVector(velocity);

	body->setVelocity(velocity);
}

void Tank::zeroVelocity()
{
	body->zeroVelocity();
}

void Tank::rotateTurret(float input)
{
	turretAngle += input * dT;

	//cout << turretAngle << endl;
}


GUVector4* Tank::getForwardVector()
{
	GUVector4* ahead = new GUVector4(0, 0, -1, 0);

	GUQuaternion ori = body->getOrientation();

	ori.rotateVector(*ahead);

	return ahead;
}

GameObject* Tank::getBodyGameObject()
{
	return body;
}

float Tank::getForwardVelocity()
{
	return forwardVelocity;
}


void Tank::update(float dT)
{
	this->dT = dT;

	gearDelay -= dT;

	if (gearDelay < 0)
		changingGear = false;

	float signlessV = abs(forwardVelocity);

	gears(signlessV);
	brakes();
	tracks(signlessV);

	moveTank(signlessV);

	GUQuaternion bodyOri = body->getOrientation();

	GUQuaternion rotation = GUQuaternion(GUAxisAngle(0, 1, 0, turretAngle));

	turret->setOrientation(bodyOri * rotation);
	turret->setPosition(body->getPosition());

	// set these back to zero, then they are updated if called
	input_accel = 0;
	input_turn = 0;

}


void Tank::gears(float signlessV)
{
	if (cruiseControlActive)
	{
		if (input_accel < 0)
			cruiseControlActive = false;

		else if (forwardVelocity < cruiseControl)
			input_accel = 1;

	}

    aaEngine->setProperty(GAIN, 0.4f);
    aaTurbo->setProperty(GAIN, 0.05f);

    if (signlessV < 1)
    {
        aaEngine->setProperty(PITCH, 0.8f);
		aaTurbo->setProperty(PITCH, 0);
    }

    bool throttleApplied = false;


    if (forwardVelocity > 0)  // throttle isApplied if player is accel in dir of travel
    {
		
       if (input_accel > 0) throttleApplied = true;
    }
    else if (forwardVelocity < 0)
    {
        if (input_accel < 0) throttleApplied = true;
    }

	if (throttleApplied)
	{
		aaEngine->setProperty(GAIN, 0.75f);
		aaTurbo->setProperty(GAIN, 0.08f);
		if (turboSpeed < 2)
			turboSpeed += dT;
	}

	if ((!throttleApplied) && turboSpeed > 0)
	{
		turboSpeed -= dT * 3;
	}

	turbo(signlessV);


	switch (gear)
	{
	case 0:

		if (!changingGear)
		{
			if (signlessV > gearMax[0])
			{
				changeGear(1);
				changingUp = true;
				break;
			}
			aaEngine->setProperty(PITCH, mapFunc(gearMin[gear], gearMax[gear], 0.8f, 1.40f, signlessV));
		}
		else if (!changingUp)
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 1.4f, 0.8f, gearDelay));
		}
		else
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 0.8f, 1.4f, gearDelay));
		}

		break;

	case 1:

		if (!changingGear)
		{
			if (signlessV < gearMin[1])
			{
				changingUp = false;
				changeGear(-1);
				break;
			}
			if (signlessV > gearMax[1])
			{
				changeGear(1);
				changingUp = true;
				break;
			}
			 aaEngine->setProperty(PITCH, mapFunc(gearMin[gear], gearMax[gear], 0.8f, 1.40f, signlessV));
		}
		else if (!changingUp)
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 1.4f, 0.8f, gearDelay));
		}
		else
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 0.8f, 1.4f, gearDelay));
		}

		break;


	case 2:

		if (!changingGear)
		{
			if (signlessV < gearMin[2])
			{
				changingUp = false;
				changeGear(-1);
				break;
			}

			if (signlessV > gearMax[2])
			{
				changeGear(1);
				changingUp = true;
				break;
			}
			aaEngine->setProperty(PITCH, mapFunc(gearMin[gear], gearMax[gear], 0.8f, 1.40f, signlessV));
		}
		else if (!changingUp)
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 1.4f, 0.8f, gearDelay));
		}
		else
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 0.8f, 1.4f, gearDelay));
		}

		break;

	case 3:

		if (!changingGear)
		{
			if (signlessV < gearMin[3])
			{
				changingUp = false;
				changeGear(-1);
				break;
			}
			aaEngine->setProperty(PITCH, mapFunc(gearMin[gear], gearMax[gear], 0.8f, 1.40f, signlessV));
		}
		else if (!changingUp)
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 1.4f, 0.8f, gearDelay));
		}
		else
		{
			aaEngine->setProperty(PITCH, mapFunc(0, gearDelayMax, 0.8f, 1.4f, gearDelay));
		}

	}

}

void Tank::turbo(float signlessV)
{
	float pitch;

	if (signlessV > boostHi[gear])
	{

		pitch = turboSpeed * 2.0f;
		if (pitch < 1.2f) pitch = 0;
		if (pitch > 2) pitch = 2;
		aaTurbo->setProperty(PITCH, pitch);

		return;
	}

	turboSpeedMax = mapFunc(boostLow[gear], boostHi[gear], 0, 1, signlessV);

	if (turboSpeed > turboSpeedMax) 
		turboSpeed = turboSpeedMax;

	pitch = mapFunc(0, 1, 0, 2.0f, turboSpeed);
	if (pitch < 1.2f) pitch = 0;

	aaTurbo->setProperty(PITCH, pitch);
}

void Tank::brakes()
{
	if (!brakesOn)
	{
		aaBrakes->setProperty(GAIN, 0);
		return;
	}

	float signlessV = abs(forwardVelocity);

	if (signlessV > 2)
	{
		aaBrakes->setProperty(GAIN, 0.3f);
	}
	else
	{
		aaBrakes->setProperty(GAIN, mapFunc(0, 2, 0, 0.3f, signlessV));
	}
}

void Tank::tracks(float signlessV)
{
	float pitchMax = 1.6f;

	float vol = mapFunc(0, 52.5f, 0.6f, 0.8f, signlessV);
	if (vol > 1) vol = 1;

	float pitch = mapFunc(0, 52.5f, 0.6f, pitchMax, signlessV);

	if (pitch > pitchMax) pitch = pitchMax;

	if (brakesOn) vol = 0;


	if (signlessV < 1) vol = mapFunc(0, 1, 0, 0.4f, signlessV); ;

	aaTracks->setProperty(GAIN, vol);
	aaTracks->setProperty(PITCH, pitch);
}

void Tank::moveTank(float signlessV)
{


	// When player turns, it is multiplied by a scalar, turnVelocity, initially set to zero.
	// turn Velocity is incremented by delta time each frame, up to value of turnVelocityMax
	// If player stops turning, or turns in other direciton, turn velocity reverts to zero. 
	// Because of the way I approach the turning of the tank, this needs to be defined for 
	// each direction (forward, reverse), else you wouldnt be able to turn while reversing!
	if (input_turn < 0) // right
	{
		if (forwardVelocity > 0)
		{
			if (currentTurn < 0)
			{
				turnVelocity = 0;
			}
		}
		else
		{
			if (currentTurn > 0)
			{
				turnVelocity = 0;
			}
		}

	}
	else if (input_turn > 0) // left
	{
		if (forwardVelocity > 0)
		{
			if (currentTurn > 0)
			{
				turnVelocity = 0;
			}
		}
		else
		{
			if (currentTurn < 0)
			{
				turnVelocity = 0;
			}
		}
	}
	else // input_turn == 0  Not turning
	{
		turnVelocity = 0;
	}


	// We now set the accelerations to zero, ready for calculation
	accelAhead = currentTurn = 0;

	

	if (!brakesOn)
	{
		if (input_accel > 0) // IF VEHICLE IS ACCELERATING FORWARD
		{
			if (input_turn > 0) // TURNING LEFT
			{
				accelAhead = 1;
				currentTurn = -1;
			}
			else if (input_turn < 0) // TURNING RIGHT
			{
				accelAhead = 1;
				currentTurn = 1;
			}
			else // NOT TURNING
			{
				if (forwardVelocity > 0)
				{
					accelAhead = 1.4f; // if vehichle is currently travelling forward
				}
				else
				{
					accelAhead = 2.4f; // vehicle travelling in reverse
				}
			}
		}
		else if (input_accel < 0) // IF VEHICLE IS ACCELERATING BACKWARD
		{

			if (input_turn > 0) // TURNING LEFT 			
			{
				if (forwardVelocity > 0) // TRAVELLING FORWARD
				{
					accelAhead = -1;
					currentTurn = -1;
				}
				else
				{
					accelAhead = -1;
					currentTurn = 1;
				}
			}
			else if (input_turn < 0) // TURNING RIGHT
			{
				if (forwardVelocity > 0)
				{
					accelAhead = -1;
					currentTurn = 1;
				}
				else
				{
					accelAhead = -1;
					currentTurn = -1;
				}
			}
			else // no turn
			{
				if (forwardVelocity > 0)
				{
					accelAhead = -2.4f;
				}
				else
				{
					accelAhead = -1.4f;
				}
			}
		}
		else if (input_turn > 0) // NO FWD/REV ACCEL, TURNING LEFT
		{
			if (abs(forwardVelocity) < 2)
			{
				aaEngine->setProperty(PITCH, 1);
				aaTracks->setProperty(GAIN, 0.7f);
			}
		}
		else if (input_turn < 0) // NO FWD/REV ACCEL, TURNING RIGHT
		{
			if (abs(forwardVelocity) < 2)
			{
				aaEngine->setProperty(PITCH, 1);
				aaTracks->setProperty(GAIN, 0.7f);
			}
		}
	}

	if (abs(forwardVelocity) > topSpeed) input_accel = 0;

	//cout << "v/vMax" << forwardVelocity << " / " << topSpeed << endl;

	if (brakesOn && signlessV < 0.5) forwardVelocity = 0;

	switch (gear)
	{
	case 0:
		power = 5 - mapFunc(gearMin[0], gearMax[0], 0, 1, signlessV);
		break;

	case 1:
		power = 4 - mapFunc(gearMin[1], gearMax[1], 0, 1, signlessV);
		break;

	case 2:
		power = 2 - mapFunc(gearMin[2], gearMax[2], 0, 1, signlessV);
		break;
	case 3:
		power = 1 - mapFunc(gearMin[3], gearMax[3], 0, 1, signlessV);
		break;
	}

	power *= (turboSpeed);
	bool reverseThrottleApplied = false;

	if (forwardVelocity > 0)
	{
		if (input_accel < 0) reverseThrottleApplied = true;
	}
	else if (forwardVelocity < 0)
	{
		if (input_accel > 0) reverseThrottleApplied = true;
	}

	if (reverseThrottleApplied) power = 3;

	if (power < 1) power = 1;

	if (changingGear)
	{
		if (forwardVelocity > 0)
		{
			power = -0.4f;
		}
		else
		{
			power = 0.4f;
		}
	}

	if (signlessV < minSpeed && !input_accel)
	{
		// If not accelerating, and below minimum speed, stops vehicle. Prevents weird graphics
		forwardVelocity = 0;
		body->setVelocity(GUVector4(0, 0, 0));
	}
	else
	{
		GUVector4* vec = getForwardVector();
		*vec *= forwardVelocity;
		*vec *= dT;
		*vec *= engineTorque;

		body->setVelocity(*vec);

		delete vec;
	}

	int sign = forwardVelocity > 0 ? -1 : 1; // if moving forward, i.e > 0, we want a negative sign to apply acceleration with for braking

	if (brakesOn)
		forwardVelocity += brakeForce * dT * sign; // quickly slow down vehicle

	if (input_accel == 0)
		forwardVelocity += drag * dT * sign; // simulates rolling resistance


	float forwardAccel;
	forwardAccel = accelAhead * power * dT * abs(input_accel);
	

	forwardVelocity += forwardAccel;

	if (turnVelocity < turnVelocityMax) 
		turnVelocity += dT;

	float angle = input_turn * turnVelocity;

	turn(angle);
}

void Tank::changeGear(int shift)
{
	gear += shift;
	changingGear = true;
	gearDelay = gearDelayMax;
}



#ifndef GO_AUDIO_COMP_H_
#define GO_AUDIO_COMP_H_

#include <string>
#include <vector>

using std::string;

#include <CoreStructures\CoreStructures.h>
#include <CoreStructures/GUQuaternion.h>

using CoreStructures::GUAxisAngle;
using CoreStructures::GUMatrix4;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;

class ActionAudio;
class AudioManager;

class GOaudioComponent
{
public:

	GOaudioComponent();
	~GOaudioComponent();

	void play(string name);

	void update(GUVector4* pos, GUVector4* vel, GUQuaternion* ori);


	void addActionAudio(string clipName, string actionName);
	void playActionAudio(string actionName);
	int searchActionAudio(string name);
	void setActionAudioProperty(string actionName, int propertyID, float value);

	ActionAudio* getActionAudio(string clipName);


private:

	AudioManager* audioMgr;
	std::vector<ActionAudio*> actionAudio;

};

#endif // !GO_AUDIO_COMP_H_

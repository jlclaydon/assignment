#include "AudioSource.h"
#include "AudioManager.h"



void AudioSource::play()
{
	alSourcePlay(alSource);
	//AudioManager::getInstance()->playSound(ALsource);
}

AudioSource::AudioSource(string name)
{
	alSource = *AudioManager::getInstance()->createSource(name);
}

void AudioSource::update(GUVector4* position, GUVector4* velocity, GUQuaternion* orientation)
{
	GUQuaternion heading = orientation->inv();

	GUVector4 at(0, 0, -1, 0);
	heading.rotateVector(at);

	alSource3f(alSource, AL_POSITION, position->x, position->y, position->z);
	alSource3f(alSource, AL_VELOCITY, at.x, at.y, at.z);
	alSource3f(alSource, AL_DIRECTION, velocity->x, velocity->y, velocity->z);
}

void AudioSource::setLoop(float loop)
{
	bool _loop = false;

	if (loop)
		_loop = true;

	alSourcei(alSource, AL_LOOPING, loop);
}

void AudioSource::setPitch(float pitch)
{
	alSourcef(alSource, AL_PITCH, pitch);
	
}

void AudioSource::setGain(float gain)
{
	alSourcef(alSource, AL_GAIN, gain);
}

void AudioSource::setProperty(int id, float val)
{
	switch (id)
	{
	case 0:
		setLoop(val);
	case 1:
		setPitch(val);
		return;
	case 2:
		setGain(val);
		return;
	}

}


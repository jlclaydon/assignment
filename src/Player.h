#ifndef PLAYER_H_
#define PLAYER_H_

#include <CoreStructures\CoreStructures.h>
#include <iostream>
using std::cout; using std::endl;

using CoreStructures::GUAxisAngle;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;

class GameObject;
class Tank;

class Player
{
private:

	Tank* playerTank;	

public:

	void init(Tank* playerTank);

	void setAccel(float input);
	void setTurn(float input);

	void rotateTurret(float input);

	void zeroVelocity();


	void update(float dT);

	void setBrakes(bool brakesOn);

};

#endif
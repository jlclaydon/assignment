#ifndef CUBEMAP_TEXTURE_H_
#define  CUBEMAP_TEXTURE_H

#include <vector>
#include <iostream>
using std::cout; using std::endl;
using std::vector;
using std::string;

#include <glew\glew.h>

class CubemapTexture
{
public:

	CubemapTexture(string path, vector<string> filenames);

	void bind(GLenum TextureUnit);


private:

	vector<GLuint> textures;
	GLuint cubemapTex;

};

#endif
#include "AudioClip.h"



AudioClip::AudioClip(string filename, int frequency)
{


	name = filename;

	string path = "resources\\audio\\";
	path += filename;
	path += ".wav";

	alGenBuffers(1, &buffer);

	auto mySoundData = new GURiffModel(path.c_str());

	RiffChunk formatChunk = mySoundData->riffChunkForKey('tmf');
	RiffChunk dataChunk = mySoundData->riffChunkForKey('atad');

	WAVEFORMATEXTENSIBLE wave;
	memcpy_s(&wave, sizeof(WAVEFORMATEXTENSIBLE), formatChunk.data, formatChunk.chunkSize);


	alBufferData(buffer, AL_FORMAT_MONO16, (ALvoid*)dataChunk.data, (ALsizei)dataChunk.chunkSize, frequency);
}

ALuint* AudioClip::getBuffer()
{
	return &buffer;
}

string AudioClip::getName()
{
	return name;
}

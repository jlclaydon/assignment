#include "GOaudioComponent.h"

#include "AudioManager.h"
#include "AudioSource.h"
#include "ActionAudio.h"

GOaudioComponent::GOaudioComponent()
{
	audioMgr = AudioManager::getInstance();

}

GOaudioComponent::~GOaudioComponent()
{
}

void GOaudioComponent::play(string name)
{
	int index = searchActionAudio(name);

	if (index != -1)
	{
		actionAudio[index]->play();
	}
}

void GOaudioComponent::update(GUVector4* pos, GUVector4* vel, GUQuaternion* ori)
{
	for (int i = 0; i < actionAudio.size(); ++i)
	{
		actionAudio[i]->update(pos, vel, ori);
	}
}


void GOaudioComponent::addActionAudio(string clipName, string actionName)
{
	AudioSource* source = new AudioSource(clipName);

	ActionAudio* audio = new ActionAudio(source, actionName);

	actionAudio.push_back(audio);
}

void GOaudioComponent::playActionAudio(string actionName)
{
	int index = searchActionAudio(actionName);

	if (index != -1)
	{
		actionAudio[index]->play();		
	}
}

int GOaudioComponent::searchActionAudio(string actionName)
{
	for (int i = 0; i < actionAudio.size(); ++i)
	{
		if (strcmp(actionName.c_str(), actionAudio[i]->getActionName().c_str()) == 0)
			return i;
	}

	return -1;
}

void GOaudioComponent::setActionAudioProperty(string actionName, int propertyID, float value)
{
	int index = searchActionAudio(actionName);

	if (index != -1)
	{
		actionAudio[index]->setProperty(propertyID, value);
	}
	else
	{
		cout << actionName << " is not attached to this game object!" << endl;
	}


}

ActionAudio* GOaudioComponent::getActionAudio(string clipName)
{
	int index = searchActionAudio(clipName);

	if (index != -1)
	{
		return actionAudio[index];
	}
	else
	{
		cout << clipName << " not attached to this game object!" << endl;
	}

	return nullptr;
}

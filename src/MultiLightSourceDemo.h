#pragma once
#include "Game.h"

class MultipleLightSourceShader;

class MultiLightSourceDemo :
    public Game
{

public:

	void setupAssets();

	void init();

	void gameLoop();

	void freeFunction(int id);

	void display();

private:


	MultipleLightSourceShader* shader;
	GUMatrix4 lightX;
	GUMatrix4 lightY;
	GUMatrix4 lightZ;
	GUMatrix4 lightR;
	GUMatrix4 lightG;
	GUMatrix4 lightB;
	GUMatrix4 lightAttI;
	GUMatrix4 lightAttII;
	GUMatrix4 lightAttIII;

};


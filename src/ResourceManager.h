#ifndef RESOURCE_MGR_H_
#define RESOURCE_MGR_H_

#include <iostream>
#include <vector>

#include "GOmeshComponent.h"
#include <CoreStructures\CoreStructures.h>

using std::string;
using std::vector;
using std::cout; using std::endl;

using CoreStructures::GUAxisAngle;
using CoreStructures::GUMatrix4;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;

class ResourceManager
{

private:

	ResourceManager(); // private constructor
	static ResourceManager* c_resourceManager;

	void importModel(string name);

	vector<GOmeshComponent*> meshes;

	int searchMeshes(string name, string textureNameExt);

	GOmeshComponent* createMeshComponent(string name, string texNameAndExt);

public:

	~ResourceManager();

	GOmeshComponent* getOrCreate(string name, string texNameAndExt);

	static ResourceManager* getInstance();

};

#endif
#include "ActionAudio.h"
#include "AudioSource.h"

void ActionAudio::update(GUVector4* pos, GUVector4* vel, GUQuaternion* ori)
{
	source->update(pos, vel, ori);
}

ActionAudio::ActionAudio(AudioSource* source, string actionName)
{
	this->source = source;
	this->actionName = actionName;
}

string ActionAudio::getActionName()
{
    return actionName;
}

void ActionAudio::play()
{
	source->play();
}

void ActionAudio::setProperty(int id, float value)
{
	source->setProperty(id, value);
}



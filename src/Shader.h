#ifndef SHADER_H_
#define SHADER_H_

#include <string>
using std::string;

#include <glew/glew.h>
#include <CoreStructures\CoreStructures.h>

using CoreStructures::GUAxisAngle;
using CoreStructures::GUMatrix4;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;

class Shader
{
public:

	GLuint s;

	virtual void render() = 0;



private:


};

#endif
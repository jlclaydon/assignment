#ifndef GAME_OBJECT_H_
#define GAME_OBJECT_H_

#include <iostream>
#include <vector>

using std::string;

#include "AABB.h"
#include "GOmeshComponent.h"


#include <CoreStructures\CoreStructures.h>
//#include <CoreStructures/GUQuaternion.h>

using CoreStructures::GUAxisAngle;
using CoreStructures::GUMatrix4;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;


class GOaudioComponent;

class GameObject
{
public:
	


	GameObject(string meshName, string texNameAndExtension, GUVector4 pos, GUAxisAngle ori, GUVector4 scale, GLuint* shaderProgram);
	~GameObject();

	void update(float deltaTime);

	GLuint* getTexture();

	GUVector4 getPosition();

	void setPosition(GUVector4 position);

	GUVector4 getScale();
	void setScale(GUVector4 scale);

	GUQuaternion getOrientation();
	void setOrientation(GUQuaternion orientation);

	// We need a separate method for corrected orientation, since the GameObject class
	// requires the raw orientation (initially zero for all objects), 
	// while renderer needs it offset to its initial orientation!
	GUQuaternion getOffsetOrientation();

	GUVector4 getVelocity();
	void setVelocity(GUVector4 velocity);

	void zeroVelocity();

	void drawAABB(bool draw = true);

	void setShaderProgram(GLuint* shaderProgram);

	GOaudioComponent* getAudioComponent();

	void render(GUMatrix4 camMatrix); // using external shader
	void render(GUMatrix4 camMatrix, GUVector4 lightSource, GUVector4 lightCol);

	bool transparency = false;
	float alpha = 1.0f;
	int lightBoost = 1;
	bool isTransparent();
	void setTransparency(bool transparency);
	void setAlpha(float alpha);

	void setLightBoost(int lightBoost);
	int getLightBoost();

	GUMatrix4 getMVP(GUMatrix4 camMatrix);
	GUMatrix4 getWorldScaledMatrix();
	GUMatrix4 getWorldMatrix();
	
private:



	GOmeshComponent* meshComponent;
	GOaudioComponent* audioComponent;

	GUVector4 scale;
	GUVector4 position;
	GUQuaternion orientation;
	GUQuaternion initialOri;
	GUVector4 velocity;

	AABB* boundingBox;
	bool drawBoundingBox;

	GLuint* shaderProgram;

	GLuint standard_mvpLoc;
	GLuint standard_lightCol;
	GLuint standard_lightDir;
	GLuint lightboost_factor;
	GLuint alpha_factor;

	bool externalShader = false;

};

#endif 
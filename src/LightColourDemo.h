#ifndef LIGHT_COL_DEMO_H
#define LIGHT_COL_DEMO_H

#include "Game.h"

class SpotlightShader;

class LightColourDemo : public Game
{
public:

	void setupAssets();

	void init();

	void gameLoop();

	void freeFunction(int id);

	void update();

	void display();

private:

	void adjustRed(float input);
	void adjustGreen(float input);
	void adjustBlue(float input);
	void printCols();


	SpotlightShader* shader;
	GUVector4 att;
	GUVector4 lightPos;
};

#endif

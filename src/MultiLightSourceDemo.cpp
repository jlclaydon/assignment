#include "MultiLightSourceDemo.h"

#include "AudioManager.h"
#include "CameraController.h"
#include "InputManager.h"
#include "GameObject.h"
#include "Player.h"
#include "SkyBox.h"
#include "Tank.h"
#include "TimeUtil.h"

#include "MultipleLightSourceShader.h"

void MultiLightSourceDemo::setupAssets()
{
	GameObject* turntable = new GameObject("disc", "granite.jpg",
		GUVector4(0, -0.10, 8), GUAxisAngle(0, 0, 0, 0), GUVector4(100, 1, 100), &standardShader);
	
	gameObjects.push_back(turntable);


	
}

void MultiLightSourceDemo::init()
{
	Game::init();

	lightX = GUMatrix4();
	lightY = GUMatrix4();
	lightZ = GUMatrix4();
	lightR = GUMatrix4();
	lightG = GUMatrix4();
	lightB = GUMatrix4();
	lightAttI = GUMatrix4();
	lightAttII = GUMatrix4();
	lightAttIII = GUMatrix4();


	for (int i = 0; i < 4; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			int index = ((i * 4) + j);

			lightX(i, j) = index % 2 ? 3 : -3;
			lightY(i, j) = 2;
			lightZ(i, j) = index * 4;
			lightR(i, j) = index % 2? 1 : 0;
			lightG(i, j) = 0;
			lightB(i, j) = index % 2 ? 0 : 1;
			lightAttI(i, j) = 1;
			lightAttII(i, j) = 0.05;
			lightAttIII(i, j) = 0.01;
		}
	}

	skybox = new SkyBox("resources\\textures\\skybox\\cloud\\", cloudBox);

	setupPlayer("camo.png");
	playerGO->setPosition(GUVector4(0, 0, 0));

	shader = new MultipleLightSourceShader();
	shader->init();

	setupAssets();
}

void MultiLightSourceDemo::gameLoop()
{
	while (!quit)
	{
		update();
		display();
	}
}

void MultiLightSourceDemo::freeFunction(int id)
{
}

void MultiLightSourceDemo::display()
{
	GUMatrix4 t2 = mainCamera->projectionTransform() * camCon->getOrientation(); // Get camera orientation and view matrix
	GUMatrix4 rot = GUMatrix4(GUQuaternion(GUAxisAngle(1, 0, 0, PI))); // A rotation matrix, for flipping 180 degrees about the X
	t2 *= rot; // apply rotation to t2

	//4
	skybox->render(t2);

	GUMatrix4 T = getCameraMatrix();

	// Render all GameObjects
	for (int i = 0; i < gameObjects.size(); ++i)
	{
		
		shader->update(gameObjects[i]->getMVP(T),
			gameObjects[i]->getWorldMatrix(),
			lightX, lightY, lightZ,
			lightR, lightG, lightB, 
			lightAttI, lightAttII, lightAttIII,
			gameObjects[i]->getLightBoost());

		gameObjects[i]->render(T);
	}

	//cout << playerGO->getPosition().z << endl;

	SDL_GL_SwapWindow(sdl_window);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

}



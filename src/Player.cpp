#include "Player.h"

#include "GameObject.h"
#include "Tank.h"

void Player::init(Tank* playerTank)
{
	this->playerTank = playerTank;
}

void Player::setAccel(float input)
{
	// calculate a basis vector for forward in respect to tanks orientation
	playerTank->setAccel(input);
}

void Player::setTurn(float input)
{
	playerTank->setTurn(input);
}

void Player::rotateTurret(float input)
{
	playerTank->rotateTurret(input);
}

void Player::zeroVelocity()
{
	playerTank->zeroVelocity();
}

void Player::update(float dT)
{
	playerTank->update(dT);
}

void Player::setBrakes(bool brakesOn)
{
	playerTank->setBrakes(brakesOn);
}




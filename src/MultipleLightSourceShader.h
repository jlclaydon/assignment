#ifndef MULTI_SRC_SHDR_H
#define MULTI_SRC_SHDR_H

#include "Shader.h"
#include <iostream>
using std::cout; using std::endl;


class MultipleLightSourceShader : public Shader
{

public:

	void render();

	void update(GUMatrix4 mvp, GUMatrix4 lt, GUMatrix4 lx, GUMatrix4 ly, GUMatrix4 lz, GUMatrix4 lr, GUMatrix4 lg, GUMatrix4 lb, GUMatrix4 l1, GUMatrix4 l2, GUMatrix4 l3, float factor);

	void init();


private:

	GLuint shaderProgram;
	GLuint mvpLoc;
	GLuint lightMatrix;
	GLuint lightPosX;
	GLuint lightPosY;
	GLuint lightPosZ;
	GLuint lightColR;
	GLuint lightColG;
	GLuint lightColB;
	GLuint lightAttI; // Not sure what these attenuation properties are actually called
	GLuint lightAttII;
	GLuint lightAttIII;

	GLuint _factor;

};

#endif
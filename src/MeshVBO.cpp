#include "MeshVBO.h"


MeshVBO::MeshVBO()
{
	
}

MeshVBO::~MeshVBO()
{

}

void MeshVBO::render()
{
	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, faceCount * 3, GL_UNSIGNED_INT, (void*)0);
}

void MeshVBO::init(const aiScene* scene, int index)
{
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);

	// position vbo
	glGenBuffers(1, &vertexPos);
	glBindBuffer(GL_ARRAY_BUFFER, vertexPos);
	glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[index]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[index]->mVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(0);

	
	// texture coordinate vbo
	glGenBuffers(1, &texCoords);
	glBindBuffer(GL_ARRAY_BUFFER, texCoords);
	glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[index]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[index]->mTextureCoords[0], GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_TRUE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(1);

	// face normals vbo
	glGenBuffers(1, &faceNormals);
	glBindBuffer(GL_ARRAY_BUFFER, faceNormals);
	glBufferData(GL_ARRAY_BUFFER, scene->mMeshes[index]->mNumVertices * sizeof(aiVector3D), scene->mMeshes[index]->mNormals, GL_STATIC_DRAW);
	glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
	glEnableVertexAttribArray(2);

	// face index vbo
	glGenBuffers(1, &faceIndicies);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, faceIndicies);

	unsigned int numBytes = scene->mMeshes[index]->mNumFaces * 3 * sizeof(unsigned int);

	unsigned int* FaceIndexArray = (unsigned int*) malloc(numBytes);

	for (int f = 0, dstIndex = 0; f < scene->mMeshes[index]->mNumFaces; f++) {

		unsigned int* I = scene->mMeshes[index]->mFaces[f].mIndices;

		FaceIndexArray[dstIndex] = I[0];
		dstIndex += 1;
		FaceIndexArray[dstIndex] = I[1];
		dstIndex += 1;
		FaceIndexArray[dstIndex] = I[2];
		dstIndex += 1;

	}

	glBufferData(GL_ELEMENT_ARRAY_BUFFER, numBytes, FaceIndexArray, GL_STATIC_DRAW);

	faceCount = scene->mMeshes[index]->mNumFaces;  

	
	
}

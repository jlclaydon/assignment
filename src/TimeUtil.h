#ifndef TIME_UTIL_H_
#define TIME_UTIL_H_

#include "SDL2Common.h"
#include <string>

using std::string;

class TimeUtil
{
private:
    unsigned int timeAtBoot;

    unsigned int currentTimeIndex;
    unsigned int prevTimeIndex;
    unsigned int timeDelta;
    float timeDeltaInSeconds;


public:

    TimeUtil();
    ~TimeUtil();

    void update();

    float getTimeDelta();

    unsigned int getCurrentTimeIndex();
    
    string getTimeElapsedString();

};

#endif


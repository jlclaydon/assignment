#ifndef INPUT_MGR_H
#define INPUT_MGR_H

#include "SDL2Common.h"
#include <CoreStructures\CoreStructures.h>

using CoreStructures::GUPivotCamera;

class AudioManager;
class CameraController;
class Player;
class Game;

class Tank;

class InputManager
{
private:

	Tank* enemy;

	AudioManager* audioMgr;
	CameraController* camCon;
	Player* player;
	Game* game;

	void devMode();
	void mouseInput();
	void menuKeys();
	void playerKeys();
	void gamepadControls();

	float* turretY;


	const Uint8* keyStates;

	SDL_Joystick* gamepad;
	const int SDL_AXIS_MAX_VAL = 32767;
	int deadzone = 5000;

	GUPivotCamera* mainCamera = nullptr;

	

public:

	InputManager();
	~InputManager();

	void init(CameraController* camCon, Game* game, Player* player);


	void processInputs();
};


#endif // !USERINPUT_H_




#include "AudioManager.h"
#include "AudioClip.h"

AudioManager* AudioManager::c_audioManager = NULL;

int AudioManager::searchClips(string name)
{
	for (int i = 0; i < audioClips.size(); ++i)
	{
		if (strcmp(name.c_str(), audioClips[i]->getName().c_str()) == 0)
			return i;
	}

	return -1;
}

AudioManager::AudioManager()
{
	alcDevice = nullptr;
	alcContext = nullptr;
}

void AudioManager::loadClip(string name, int frequency)
{
	AudioClip* newClip = new AudioClip(name, frequency);

	audioClips.push_back(newClip);
}

AudioManager::~AudioManager()
{
	
}

AudioManager* AudioManager::getInstance()
{
	if (c_audioManager == NULL)
	{
		cout << "creating audio manager...\n";
		c_audioManager = new AudioManager();
	}
	return c_audioManager;
}

void AudioManager::init()
{

	// Init OpenAL
	alcDevice = alcOpenDevice(NULL);
	alcContext = alcCreateContext(alcDevice, NULL);
	alcMakeContextCurrent(alcContext);

	// Test audio clip. Load and get ptr to buffer.

	// Wave filename, Frequency
	loadClip("bgm", 88200); // background music
	loadClip("heavy_brake_squeal", 88200); // tank brakes
	loadClip("tracks", 88200); // tank tracks
	loadClip("heavy_diesel", 44100); // engine
	loadClip("whistle", 44100); // turbo 
		
}

void AudioManager::update(GUVector4* listenerPos, GUQuaternion* listenerOri, GUVector4* listenerVelocity)
{
	ALfloat pos[] = { listenerPos->x, listenerPos->y, listenerPos->z };
	alListenerfv(AL_POSITION, pos);

	GUQuaternion flipCam = listenerOri->inv();
			
	GUVector4 at(0, 0, -1, 0);
	GUVector4 up(0, 1, 0, 0);
	flipCam.rotateVector(at);
	flipCam.rotateVector(up);

	ALfloat ori[] = { at.x, at.y, at.z, up.x, up.y, up.z };
	alListenerfv(AL_ORIENTATION, ori);
	
	ALfloat vel[] = { listenerVelocity->x, listenerVelocity->y, listenerVelocity->z };
	alListenerfv(AL_VELOCITY, vel);	
}

void AudioManager::test()
{
	
}

void AudioManager::playSound(ALuint source)
{
	alSourcePlay(source);
}

ALuint* AudioManager::createSource(string name)
{
	ALuint* audio = getClip(name);

	ALuint source;
	alGenSources(1, &source);
	alSourcei(source, AL_BUFFER, *audio);

	return &source;
}

void AudioManager::updateSource(ALuint source, GUVector4* pos, GUVector4* vel, GUQuaternion* ori)
{

	GUQuaternion heading = ori->inv();

	GUVector4 at(0, 0, -1, 0);
	heading.rotateVector(at);
	
	alSource3f(source, AL_POSITION, pos->x, pos->y, pos->z);
	alSource3f(source, AL_VELOCITY, at.x, at.y, at.z);
	alSource3f(source, AL_DIRECTION, vel->x, vel->y, vel->z);
}

ALuint* AudioManager::getClip(string name)
{
	return audioClips[searchClips(name)]->getBuffer();	// naughty, need to fix
}




#ifndef AUDIO_CLIP_H_
#define AUDIO_CLIP_H_

#include "GURiffModel.h"
#include "al.h"
#include "alc.h"
#include "efx.h"
#include "EFX-Util.h"
#include "efx-creative.h"
#include <xram.h>
#include <mmreg.h>

#include <iostream>
using std::cout; using std::endl;
using std::string;


class AudioClip
{
private:

	ALuint buffer;

	string name;

public:

	AudioClip(string filename, int frequency);

	ALuint* getBuffer();

	string getName();

};

#endif
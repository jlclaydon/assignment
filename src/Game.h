#ifndef TANK_GAME_H_
#define TANK_GAME_H_

#define PI 3.14159265359

// stl 
#include <iostream>
#include <string>
#include <vector>

using std::string;
using std::vector;

// base project
#include <glew/glew.h>
#include <glew/wglew.h>
#include <GL\freeglut.h> // still needed for textures
#include <CoreStructures\CoreStructures.h>
#include "CGTexturedQuad.h"
#include "CGPrincipleAxes.h"
#include "shader_setup.h"

using CoreStructures::GUPivotCamera;
using CoreStructures::GUClock;
using CoreStructures::GUQuaternion;
using CoreStructures::GUMatrix4;
using CoreStructures::GUVector4;

// other libs
#include "SDL2Common.h"

// my classes
class AudioManager;
class CameraController;
class InputManager;
class GameObject;
class Player;
class ResourceManager;
class SkyBox;
class Tank;
class TimeUtil;

class Game
{
protected:

	vector<string> Game::cloudBox // skybox. should use a manager ig
	{
		"right.png",
		"left.png",
		"top.png",
		"bottom.png",
		"front.png",
		"back.png"
	};

	bool quit = false; // Flag for terminating program

	// SDL vars
	SDL_Window* sdl_window;
	const int SDL_OK = 0;
	int WINDOW_WIDTH = 1200;
	int WINDOW_HEIGHT = 900;
		
	AudioManager* audioMgr;
	InputManager* inputMgr;
	TimeUtil* timeUtil;

	GUClock* mainClock = nullptr;

	// Camera
	CameraController* camCon;
	GUPivotCamera* mainCamera = nullptr;
	GUQuaternion* cameraOrientation;

	CGPrincipleAxes* principleAxes = nullptr;

	// Player variables
	Player* player;
	GameObject* playerGO;
	float shermanScale = 0.4f;

	// Scene/game variables
	float dT = 0.0f;
	float fov = 45;
	vector<GameObject*> gameObjects;
	SkyBox* skybox;
	GUVector4 theSun;
	GUVector4 lightCol;

	// Shader programs
	GLuint basicShader;
	GLuint standardShader;
	GLuint lightboostShader;
	GLuint transparencyShader;
	GLuint skyboxShader;
;
	// Shader vars
	GLuint standard_lightCol;
	GLuint standard_lightDir;
	GLuint standard_mvpLoc;
	GLuint lightboost_factor;
	GLuint alpha_factor;

	// Internal methods
	void initSDL(); 
	void update(void); 
	void display(void); 
	void shaderSetup();
	void setupPlayer(string texture);
	
	// Event handling functions
	void reportContextVersion(void);
	void reportExtensions(void);	
	
public:

	~Game();

	void shutdown();

	void init();
	virtual void gameLoop() = 0;

	void callQuit();
	void setCamFOV(float input);

	virtual void freeFunction(int id) = 0;

	GUMatrix4 getCameraMatrix();
};


#endif
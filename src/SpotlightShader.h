#ifndef SPOTLIGHT_SHDR_H
#define SPOTLIGHT_SHDR_H

#include "Shader.h"


class SpotlightShader : public Shader
{

public:

	void render();

	void update(GUMatrix4 mvp, GUVector4 pos, GUVector4 col, GUVector4 att, int lightBoost);

	void init();


private:

	void* _render;

	GLuint shaderProgram;
	GLuint mvpLoc;
	GLuint lightPos;
	GLuint lightCol;
	GLuint lightAtt;
	GLuint factor;

};

#endif
#include "InputManager.h"

#include "AudioManager.h"
#include "CameraController.h"
#include "Player.h"
#include "Game.h"

#include <iostream>

#include "Tank.h"

InputManager::InputManager()
{

}

InputManager::~InputManager()
{

}

void InputManager::init(CameraController* camCon, Game* game, Player* player)
{
   
    this->camCon = camCon;
    this->game = game;
    this->player = player;
    this->audioMgr = AudioManager::getInstance();


    keyStates = SDL_GetKeyboardState(NULL);
    SDL_Init(SDL_INIT_JOYSTICK | SDL_INIT_NOPARACHUTE);
    SDL_JoystickEventState(SDL_ENABLE);

    int numConnectedGamepads = SDL_NumJoysticks();

    if (numConnectedGamepads)
    {
        gamepad = SDL_JoystickOpen(0);
    }
}

void InputManager::devMode()
{    
    if (keyStates[SDL_SCANCODE_0])
    {
        game->setCamFOV(-1);
    }

    if (keyStates[SDL_SCANCODE_2])
    {
        game->setCamFOV(1);
    }



    if (keyStates[SDL_SCANCODE_KP_0])
    {
        game->freeFunction(0);
    }

    for (int i = SDL_SCANCODE_KP_1, j = 1; i <= SDL_SCANCODE_KP_9; ++i, ++j)
    {
        if (keyStates[i])
            game->freeFunction(j);
    }      
}

void InputManager::mouseInput()
{
    int x, y;
    SDL_GetMouseState(&x, &y);
}

void InputManager::menuKeys()
{
    SDL_Event event;

    if (SDL_PollEvent(&event))  // test for events
    {
        switch (event.type)
        {
        case SDL_QUIT:
           
            game->callQuit();
          
            std::cout << "quit called from TankGame21 [X]" << std::endl;
            break;

            // Key pressed event
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
            
                game->callQuit();

                std::cout << "quit called from TankGame21 [esc]" << std::endl;
                break;
            }
            break;

            // Key released event
        case SDL_KEYUP:
            switch (event.key.keysym.sym)
            {
            case SDLK_ESCAPE:
                //  Nothing to do here.      
                break;
            }
            break;

        default:
            // not an error, there's lots we don't handle. 
            break;
        }
    }
}

void InputManager::playerKeys()
{

    if (keyStates[SDL_SCANCODE_A]) { player->setTurn(1); }

    if (keyStates[SDL_SCANCODE_D]) { player->setTurn(-1); }


    if (keyStates[SDL_SCANCODE_W]) { player->setAccel(1); }

    if (keyStates[SDL_SCANCODE_S]) { player->setAccel(-1); }

    if (keyStates[SDL_SCANCODE_R])
    {
        camCon->resetCam();
    }

    if (keyStates[SDL_SCANCODE_J])
    {
        player->rotateTurret(1);
        camCon->cameraRotY(1);
    }

    if (keyStates[SDL_SCANCODE_L])
    {
        player->rotateTurret(-1);
        camCon->cameraRotY(-1);
    }

    if (keyStates[SDL_SCANCODE_I])
    {
        camCon->cameraRotX(1);
    }

    if (keyStates[SDL_SCANCODE_K])
    {
        camCon->cameraRotX(-1);
    }



    // Camera (cursor keys)
    if (keyStates[SDL_SCANCODE_LEFT]) {  }
    if (keyStates[SDL_SCANCODE_RIGHT]) { }
    if (keyStates[SDL_SCANCODE_UP]) {   }
    if (keyStates[SDL_SCANCODE_DOWN]) {  }



    if (keyStates[SDL_SCANCODE_U]) { camCon->cameraPosZ(1); }

    if (keyStates[SDL_SCANCODE_O]) { camCon->cameraPosZ(-1); }

  

    // number keys    
    player->setBrakes(false);
    if (keyStates[SDL_SCANCODE_SPACE]) { player->setBrakes(true); }

    if (keyStates[SDL_SCANCODE_1]) { audioMgr->test(); }
}

void InputManager::gamepadControls()
{

    int lts_x = SDL_JoystickGetAxis(gamepad, 0);
    int lts_y = SDL_JoystickGetAxis(gamepad, 1);
    int rts_x = SDL_JoystickGetAxis(gamepad, 2);
    int rts_y = SDL_JoystickGetAxis(gamepad, 3);

    

    if (abs(lts_x) > deadzone)
        player->setTurn(-1.0 * lts_x / SDL_AXIS_MAX_VAL);

    if (abs(lts_y) > deadzone)
        player->setAccel(-1.0 * lts_y / SDL_AXIS_MAX_VAL);

    if (abs(rts_x) > deadzone)
    {
        player->rotateTurret(-1.0 * rts_x / SDL_AXIS_MAX_VAL);
        camCon->cameraRotY(-1.0 * rts_x / SDL_AXIS_MAX_VAL);
    }

    if (abs(rts_y) > deadzone)
          camCon->cameraRotX(-1.0 * rts_y / SDL_AXIS_MAX_VAL);
    

    string str = ">> [";
    str += std::to_string(lts_x);
    str += ", ";
    str += std::to_string(lts_y);
    str += "] [";
    str += std::to_string(rts_x);
    str += ",";
    str += std::to_string(rts_y);
    str += "]                               \r";

   // cout << str;

    if (SDL_JoystickGetButton(gamepad, 3))
    {
        //	game->reloadAttributes();
        //	game->reloadStaticAssetProperties();
        //	game->configStaticAssetProperties();
    }

    if (SDL_JoystickGetButton(gamepad, 8)) {}
     
}




void InputManager::processInputs()
{
    devMode();
    menuKeys();
    mouseInput();
    playerKeys();
    gamepadControls();
}



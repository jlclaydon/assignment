#include "Game.h"

// my classes
#include "AudioManager.h"
#include "CameraController.h"
#include "InputManager.h"
#include "GameObject.h"
#include "Player.h"
#include "SkyBox.h"
#include "ResourceManager.h"
#include "Tank.h"
#include "TimeUtil.h"



Game::~Game()
{
	
}

void Game::shutdown()
{
	if (mainClock) {

		mainClock->stop();
		mainClock->reportTimingData();
		mainClock->release();
	}

	SDL_DestroyWindow(sdl_window);
	sdl_window = nullptr;

	SDL_Quit();

	delete audioMgr;
	audioMgr = nullptr;

	delete camCon;
	camCon = nullptr;

	delete inputMgr;
	inputMgr = nullptr;

	delete timeUtil;
	timeUtil = nullptr;

	delete ResourceManager::getInstance();

	delete principleAxes;
	principleAxes = nullptr;

	delete player;
	player = nullptr;

	delete playerGO;
	playerGO = nullptr;

	delete skybox;
	skybox = nullptr;
}

void Game::shaderSetup()
{
	// load and set up shaders
	standardShader = setupShaders(
		string("resources\\shaders\\directionLight.vshade"),
		string("resources\\shaders\\standard.fshade"));


	skyboxShader = setupShaders(
		string("resources\\shaders\\skybox.vshade"),
		string("resources\\shaders\\skybox.fshade"));

	basicShader = setupShaders(
		string("resources\\shaders\\basic.vshade"),
		string("resources\\shaders\\basic.fshade"));

	standard_mvpLoc = glGetUniformLocation(standardShader, "mvpMatrix");
	standard_lightCol = glGetUniformLocation(standardShader, "lightCol");
	standard_lightDir = glGetUniformLocation(standardShader, "lightDir");
	lightboost_factor = glGetUniformLocation(standardShader, "factor");
	alpha_factor = glGetUniformLocation(standardShader, "alpha");
}

void Game::setupPlayer(string texture)
{
	// Create the player tank body and turret objects...
	playerGO = new GameObject("Sherman", texture,
		GUVector4(0, 0, 3), GUAxisAngle(0, 1, 0, 0), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);
	playerGO->setLightBoost(20);

	GameObject* playerTurret = new GameObject("ShermanTurret", texture,
		GUVector4(0, 0, 0.1), GUAxisAngle(0, 1, 0, 0), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);
	playerTurret->setLightBoost(20);

	// add these to the gameObjects list so the update method gets called automatically
	gameObjects.push_back(playerGO);
	gameObjects.push_back(playerTurret);

	Tank* playerTank = new Tank();
	player->init(playerTank); // Player object needs access to the Tank* playerTank object
	playerTank->init(playerGO, playerTurret); // Tank object contains references to the two gameObjects
}

void Game::init()
{
	// Stop clock and report final timing data
	mainClock = new GUClock();

	initSDL();

	// Initialise glew
	glewInit();

	// Initialise OpenGL...
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);
	glFrontFace(GL_CCW); // Default anyway

	timeUtil = new TimeUtil();
	camCon = new CameraController();
	player = new Player();
	inputMgr = new InputManager();
	audioMgr = AudioManager::getInstance();
	audioMgr->init();
	inputMgr->init(camCon, this, player);

	theSun = GUVector4(1, 0.7, 1);
	lightCol = GUVector4(1, 1, 0.8);

	// Setup main camera	
	float viewportAspect = (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT;
	mainCamera = new GUPivotCamera(0.0f, 0.0f, 10, fov, viewportAspect, 0.1f);	

	shaderSetup();	
	//setupPlayer();

	principleAxes = new CGPrincipleAxes();		
}

void Game::initSDL()
{
	/* SDL setup, thanks Glenn Jenkins */

	int sdl_status = SDL_Init(SDL_INIT_EVERYTHING);

	if (sdl_status != SDL_OK)
	{
		cout << "SDL failed to initialise" << endl;
		exit(1);
	}

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);

	sdl_window = SDL_CreateWindow("Loading...",		 // Window title
		SDL_WINDOWPOS_UNDEFINED,				 // X position
		SDL_WINDOWPOS_UNDEFINED,				 // Y position
		WINDOW_WIDTH,							 // width
		WINDOW_HEIGHT,							 // height               
		SDL_WINDOW_OPENGL);						 // Window flags

	if (sdl_window == nullptr) throw std::runtime_error("Error - SDL could not create window\n");

	SDL_GLContext context = SDL_GL_CreateContext(sdl_window);

	if (context == nullptr) throw std::runtime_error("Error - SDL could not create OpenGL context\n");
}

void Game::update(void) 
{
	timeUtil->update();
	dT = timeUtil->getTimeDelta();
	mainClock->tick();
	inputMgr->processInputs();	
		
	for (int i = 0; i < gameObjects.size(); ++i)
	{
		gameObjects[i]->update(dT);
	}

	player->update(dT);
	camCon->update(dT, &playerGO->getPosition(), &playerGO->getOrientation());
	audioMgr->update(&playerGO->getPosition(), &camCon->getOrientation(), &playerGO->getVelocity());
		
	// Update the window title to show current frames-per-second and seconds-per-frame data
	char timingString[256];
	sprintf_s(timingString, 256, "Tank Game 21. Average fps: %.0f; Average spf: %f", mainClock->averageFPS(), mainClock->averageSPF() / 1000.0f);
	SDL_SetWindowTitle(sdl_window, timingString);

}

void Game::display(void) 
{
	// The cam projection used for the sky box
	GUMatrix4 t2 = mainCamera->projectionTransform() * camCon->getOrientation(); // Get camera orientation and view matrix
	GUMatrix4 rot = GUMatrix4(GUQuaternion(GUAxisAngle(1, 0, 0, PI))); // A rotation matrix, for flipping 180 degrees about the X
	t2 *= rot; // apply rotation to t2

	skybox->render(t2);

	// The cam projection used for game objects
	GUMatrix4 T = getCameraMatrix();

	// Render all GameObjects
	for (int i = 0; i < gameObjects.size(); ++i)
	{
		gameObjects[i]->render(T, theSun, lightCol);
	}

	// principleAxes->render(T);

	SDL_GL_SwapWindow(sdl_window);

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	return;
}

void Game::callQuit()
{
	quit = true;	
}

void Game::setCamFOV(float input)
{
	fov += input * dT;
	mainCamera->setFieldOfView(fov);
}

GUMatrix4 Game::getCameraMatrix()
{
	return mainCamera->projectionTransform()* GUMatrix4(camCon->getOrientation())* (GUMatrix4::translationMatrix(camCon->getPosition() * -1));
}


#pragma region Helper Functions

void Game::reportContextVersion(void) {

	int majorVersion, minorVersion;

	glGetIntegerv(GL_MAJOR_VERSION, &majorVersion);
	glGetIntegerv(GL_MINOR_VERSION, &minorVersion);

	cout << "OpenGL version " << majorVersion << "." << minorVersion << "\n\n";
}

void Game::reportExtensions(void) {

	cout << "Extensions supported...\n\n";

	const char* glExtensionString = (const char*)glGetString(GL_EXTENSIONS);

	char* strEnd = (char*)glExtensionString + strlen(glExtensionString);
	char* sptr = (char*)glExtensionString;

	while (sptr < strEnd) {

		int slen = (int)strcspn(sptr, " ");
		printf("%.*s\n", slen, sptr);
		sptr += slen + 1;
	}
}



#pragma endregion



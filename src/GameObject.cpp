
#include "GameObject.h"

#include "texture_loader.h"
#include "ResourceManager.h"


#include "GOaudioComponent.h"




GameObject::GameObject(string meshName, string texNameAndExtension, GUVector4 pos, GUAxisAngle ori, GUVector4 scale, GLuint* shaderProgram)
{
	meshComponent = ResourceManager::getInstance()->getOrCreate(meshName, texNameAndExtension);
	audioComponent = new GOaudioComponent();

	position = pos;
	orientation = ori; // used for tracking orientation in world space
	this->scale = scale;
	velocity = GUVector4(0, 0, 0);

	boundingBox = new AABB(1, 1, 1);
	drawBoundingBox = false;
	
	this->shaderProgram = shaderProgram;

	standard_mvpLoc = glGetUniformLocation(*shaderProgram, "mvpMatrix");
	standard_lightCol = glGetUniformLocation(*shaderProgram, "lightCol");
	standard_lightDir = glGetUniformLocation(*shaderProgram, "lightDir");
	lightboost_factor = glGetUniformLocation(*shaderProgram, "factor");
	alpha_factor = glGetUniformLocation(*shaderProgram, "alpha");
}

GameObject::~GameObject()
{

}

void GameObject::update(float dT)
{
	position = position + (velocity * dT);

	audioComponent->update(&position, &velocity, &getOrientation());
}



GLuint* GameObject::getTexture()
{
	return meshComponent->getTexture();
}

GUVector4 GameObject::getPosition()
{
	return position;
}

void GameObject::setPosition(GUVector4 position)
{
	this->position = position;
}

GUVector4 GameObject::getScale()
{
	return scale;
}

void GameObject::setScale(GUVector4 scale)
{
	this->scale = scale;
}

GUQuaternion GameObject::getOrientation()
{
	return orientation;
}

void GameObject::setOrientation(GUQuaternion orientation)
{
	this->orientation = orientation;
}


GUVector4 GameObject::getVelocity()
{
	return velocity;
}

void GameObject::setVelocity(GUVector4 velocity)
{
	this->velocity = velocity;
}

void GameObject::zeroVelocity()
{
	velocity = GUVector4(0, 0, 0);
}

void GameObject::drawAABB(bool draw)
{
	drawBoundingBox = draw;
}

void GameObject::setShaderProgram(GLuint* shaderProgram)
{
	this->shaderProgram = shaderProgram;
}



GOaudioComponent* GameObject::getAudioComponent()
{
	return audioComponent;
}



void GameObject::render(GUMatrix4 camMatrix)
{
	glBindTexture(GL_TEXTURE_2D, *getTexture());

	meshComponent->render();

	if (drawBoundingBox)
		boundingBox->render(getMVP(camMatrix));
}

void GameObject::render(GUMatrix4 camMatrix, GUVector4 lightSource, GUVector4 lightCol)
{
	GUMatrix4 mvp = getMVP(camMatrix);

	GUVector4 lp(lightSource);
	getOrientation().inv().rotateVector(lp);

	if (isTransparent())
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glDepthMask(GL_FALSE);
	}

	glUseProgram(*shaderProgram);
	glUniformMatrix4fv(standard_mvpLoc, 1, GL_FALSE, (const GLfloat*)&(mvp.M));
	glUniform3f(standard_lightCol, lightCol.x, lightCol.y, lightCol.z);
	glUniform3f(standard_lightDir, lp.x, lp.y, lp.z);
	glUniform1f(lightboost_factor, lightBoost);
	glUniform1f(alpha_factor, alpha);	
	
	glBindTexture(GL_TEXTURE_2D, *getTexture());
	
	meshComponent->render();

	if (drawBoundingBox)
		boundingBox->render(mvp);

	glDepthMask(GL_TRUE);
	glDisable(GL_BLEND);
}

bool GameObject::isTransparent()
{
	return transparency;
}

void GameObject::setTransparency(bool transparency)
{
	this->transparency = transparency;
}

void GameObject::setAlpha(float alpha)
{
	this->alpha = alpha;
}

void GameObject::setLightBoost(int lightBoost)
{
	this->lightBoost = lightBoost;
}

int GameObject::getLightBoost()
{
	return lightBoost;
}

GUMatrix4 GameObject::getMVP(GUMatrix4 camMatrix)
{
	

	// Model view projection
	GUMatrix4 modelMatrix = getWorldScaledMatrix();

	// Perspective view projection
	GUMatrix4 camSpace = camMatrix * modelMatrix;

	return camSpace;
}

GUMatrix4 GameObject::getWorldScaledMatrix()
{
	GUMatrix4 translation = GUMatrix4::translationMatrix(getPosition());

	GUVector4 scaleVec = getScale();
	GUMatrix4 scale = GUMatrix4::scaleMatrix(scaleVec.x, scaleVec.y, scaleVec.z);

	// using offset orientation, which is determined in init of game object
	GUMatrix4 orientation = GUMatrix4(getOrientation());

	// Model view projection
	return translation * scale * orientation;
}

GUMatrix4 GameObject::getWorldMatrix()
{
	GUMatrix4 translation = GUMatrix4::translationMatrix(getPosition());

	// using offset orientation, which is determined in init of game object
	GUMatrix4 orientation = GUMatrix4(getOrientation());

	// Model view projection
	return translation * orientation.inv();
}



#include "CameraController.h"

CameraController::CameraController()
{
	dT = 0;

	xRot = 0;
	xRotMin = -0.3;
	xRotMax = 0.2;

	yRot = 0;

	xRotSpeed = 1;
	yRotSpeed = 1;

	yPositionSpeed = 10;
	zPositionSpeed = 10;

	zPosMin = 2.5;

	yPosition = 1;
	zPosition = zPosMin;
}

void CameraController::update(float dT, GUVector4* playerPos, GUQuaternion* playerOri)
{
	this->dT = dT;

	// Quat for X-axis rotation
	GUQuaternion oriX = GUQuaternion(GUAxisAngle(1, 0, 0, xRot));
	GUQuaternion quatX = *playerOri * oriX;

	// Quat for Y-axis rotation
	GUQuaternion oriY = GUQuaternion(GUAxisAngle(0, 1, 0, yRot));
	GUQuaternion quatY = *playerOri * oriY;

	// Combining the two rotations with players world space orientation
	GUQuaternion newHeading = (*playerOri) * oriY * oriX; 

	// Unit vectors for behind and above (Z positive is towards screen)
	GUVector4 longitude(0, 0, 1);
	GUVector4 altitude(0, 1, 0);

	// Scale individually, how far is camera behind and above the player tanks origin
	longitude *= zPosition;
	altitude *= yPosition; 
	
	// relate these positions to our world space
	newHeading.rotateVector(longitude);
	newHeading.rotateVector(altitude);

	// Combine our camera position relative to tank, and tank world positions.
	GUVector4 combinedPvec = longitude + altitude;
	cameraPos = combinedPvec + (*playerPos);

	// combining our camera rptations with players orientation to give world space orientation
	cameraOri = oriX.inv() * oriY.inv() * playerOri->inv();


}

GUQuaternion CameraController::getOrientation()
{
	return cameraOri;
}

GUVector4 CameraController::getPosition()
{
	return cameraPos;
}

void CameraController::cameraRotX(float input)
{
	xRot += input * dT * xRotSpeed;

	if (xRot < xRotMin) xRot = xRotMin;
	else if (xRot > xRotMax) xRot = xRotMax;
}

void CameraController::cameraRotY(float input)
{
	yRot += input * dT * yRotSpeed;
}



void CameraController::cameraPosZ(float input)
{
	zPosition += input * dT * zPositionSpeed;

	if (zPosition < zPosMin) zPosition = zPosMin;

}

void CameraController::resetCam()
{
	xRot = 0;
	yPosition = 1;
	zPosition = zPosMin;
}

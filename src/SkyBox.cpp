#include "SkyBox.h"
#include "shader_setup.h"
#include "texture_loader.h"
#include <FreeImagePlus.h>


static float skyboxVertices[] = {
    // positions          
    -1.0f,  1.0f, -1.0f,0,
    -1.0f, -1.0f, -1.0f,0,
     1.0f, -1.0f, -1.0f,0,
     1.0f, -1.0f, -1.0f,0,
     1.0f,  1.0f, -1.0f,0,
    -1.0f,  1.0f, -1.0f,0,

    -1.0f, -1.0f,  1.0f,0,
    -1.0f, -1.0f, -1.0f,0,
    -1.0f,  1.0f, -1.0f,0,
    -1.0f,  1.0f, -1.0f,0,
    -1.0f,  1.0f,  1.0f,0,
    -1.0f, -1.0f,  1.0f,0,

     1.0f, -1.0f, -1.0f,0,
     1.0f, -1.0f,  1.0f,0,
     1.0f,  1.0f,  1.0f,0,
     1.0f,  1.0f,  1.0f,0,
     1.0f,  1.0f, -1.0f,0,
     1.0f, -1.0f, -1.0f,0,

    -1.0f, -1.0f,  1.0f,0,
    -1.0f,  1.0f,  1.0f,0,
     1.0f,  1.0f,  1.0f,0,
     1.0f,  1.0f,  1.0f,0,
     1.0f, -1.0f,  1.0f,0,
    -1.0f, -1.0f,  1.0f,0,

    -1.0f,  1.0f, -1.0f,0,
     1.0f,  1.0f, -1.0f,0,
     1.0f,  1.0f,  1.0f,0,
     1.0f,  1.0f,  1.0f,0,
    -1.0f,  1.0f,  1.0f,0,
    -1.0f,  1.0f, -1.0f,0,

    -1.0f, -1.0f, -1.0f,0,
    -1.0f, -1.0f,  1.0f,0,
     1.0f, -1.0f, -1.0f,0,
     1.0f, -1.0f, -1.0f,0,
    -1.0f, -1.0f,  1.0f,0,
     1.0f, -1.0f,  1.0f, 0
};


SkyBox::SkyBox(string path, vector<string> filenames)
{
    generateTextureCube(path, filenames);

    glGenVertexArrays(1, &vertexArrayObj);
    glBindVertexArray(vertexArrayObj);

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), skyboxVertices, GL_STATIC_DRAW);

    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

    glEnableVertexAttribArray(0);

    glBindVertexArray(0);
    
    shader = setupShaders(string("resources\\shaders\\skybox.vshade"), string("resources\\shaders\\skybox.fshade"));
   
    v = glGetUniformLocation(shader, "view");
}

void SkyBox::render(GUMatrix4 view)
{
    glDepthMask(GL_FALSE);
    glUseProgram(shader);
 
    glUniformMatrix4fv(v, 1, GL_FALSE, (const GLfloat*)&(view.M));
    glActiveTexture(GL_TEXTURE0);
    glBindVertexArray(vertexArrayObj);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTex);
      
    glDrawArrays(GL_TRIANGLES, 0, 36);
    glDepthMask(GL_TRUE);

}

void SkyBox::generateTextureCube(string dir, vector<string> filenames)
{
    glGenTextures(1, &cubemapTex);
    glBindTexture(GL_TEXTURE_CUBE_MAP, cubemapTex);

    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR); 
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    // Borrowed the implementation from fiTextureLoader method, rather than using directly
    // since i need the buffer, not the resultant texture.
    for (int i = 0; i < 6; ++i)
    {
        string filepath = dir;
        filepath += filenames[i];


        BOOL				fiOkay = FALSE;
        GLuint				newTexture = 0;
        fipImage			I;

        fiOkay = I.load(filepath.c_str());

        if (!fiOkay)
        {
            cout << "FreeImagePlus: Cannot open image file.\n";
        }


        fiOkay = I.convertTo32Bits();

        if (!fiOkay)
        {
            cout << "FreeImagePlus: Conversion to 24 bits successful.\n";
        }

        auto w = I.getWidth();
        auto h = I.getHeight();

        BYTE* buffer = I.accessPixels();

        if (!buffer)
        {
            cout << "FreeImagePlus: Cannot access bitmap data.\n";
        }

        int face = GL_TEXTURE_CUBE_MAP_POSITIVE_X + i;  // some times simple is best. 
        glTexImage2D(face, 0, GL_RGB, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer);


    }


}

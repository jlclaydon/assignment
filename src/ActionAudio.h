#ifndef ACTION_AUDIO_H_
#define ACTION_AUDIO_H_

#define LOOP 0
#define PITCH 1
#define GAIN 2


#include <string>
using std::string;

#include "al.h"

class AudioSource;

#include <CoreStructures\CoreStructures.h>
#include <CoreStructures/GUQuaternion.h>

using CoreStructures::GUAxisAngle;
using CoreStructures::GUMatrix4;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;

class ActionAudio
{
private:
	AudioSource* source;
	string actionName;

public:

	void update(GUVector4* pos, GUVector4* vel, GUQuaternion* ori);

	ActionAudio(AudioSource* source, string actionName);	

	string getActionName();

	void play();

	void setProperty(int id, float value);
};

#endif
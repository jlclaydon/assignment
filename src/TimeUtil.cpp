#include "TimeUtil.h"

TimeUtil::TimeUtil()
{
    // Vars for calculating dT since last frame
    currentTimeIndex = 0;
    prevTimeIndex = 0;
    timeDelta = 0;
    timeDeltaInSeconds = 0;

    timeAtBoot = SDL_GetTicks();
}

TimeUtil::~TimeUtil()
{
}

void TimeUtil::update()
{
    //Update time delta
    currentTimeIndex = SDL_GetTicks();
    timeDelta = currentTimeIndex - prevTimeIndex; //change in time in milliseconds
    timeDeltaInSeconds = timeDelta * 0.001f; //convert to change in seconds
    prevTimeIndex = currentTimeIndex; //stores current frame time for next frame


}

float TimeUtil::getTimeDelta()
{
    return timeDeltaInSeconds;
}

unsigned int TimeUtil::getCurrentTimeIndex()
{
    return currentTimeIndex;
}

string TimeUtil::getTimeElapsedString()
{
    unsigned int difference = SDL_GetTicks() - timeAtBoot;

    int diffInSeconds = difference / 1000;

    return "Runtime: " + std::to_string(diffInSeconds) + " seconds.";
}
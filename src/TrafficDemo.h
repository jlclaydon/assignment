#pragma once
#include "Game.h"


class SkyBox;

class TrafficDemo : public Game
{
public:

	void setupAssets();

	void adjustSunOrbit(float input);
	void adjustSunY(float input);
	void updateTraffic();

	void update(void);

	void init(bool traffic);

	void gameLoop();

	void freeFunction(int id);
	
private:

	vector<Tank*> npcs;

	float rad = 10;
	float theta = 0;
	float mag = 300;

	bool traffic;

	int zHi;
	int zLo; 
	int trafficPerLane;
	int span; 
	int gap; 
	
};


#include "TrafficDemo.h"

#include "AudioManager.h"
#include "GOaudioComponent.h"
#include "CameraController.h"
#include "InputManager.h"
#include "GameObject.h"
#include "Player.h"
#include "SkyBox.h"
#include "Tank.h"
#include "TimeUtil.h"
#include "ActionAudio.h"

float vertices[] = {
-0.5f, -0.5f, 0.0f,
 0.5f, -0.5f, 0.0f,
 0.0f,  0.5f, 0.0f
};


vector<string> interstellerSkybox
{
	"xneg.png",
	"xpos.png",
	"yneg.png",
	"ypos.png",
	"zneg.png",
	"zpos.png",
};


void TrafficDemo::init(bool traffic)
{
	Game::init();

	this->traffic = traffic;

	skybox = new SkyBox("resources\\textures\\skybox\\intersteller\\", interstellerSkybox);

	setupPlayer("olive.png");
//	playerGO->drawAABB();

	setupAssets();
}

void TrafficDemo::gameLoop()
{
	while (!quit)
	{
		update();
		display();
	}
	shutdown();
}

void TrafficDemo::freeFunction(int id)
{
	switch (id)
	{
	case 4: // numleft
		adjustSunOrbit(-1);
		break;
	case 6: //numRight
		adjustSunOrbit(1);
		break;
	case 8: // up
		adjustSunY(1);
		break;
	case 2:
		adjustSunY(-1);
	}
}


void TrafficDemo::setupAssets()
{
	

	// The greater floor plan. Very large
	gameObjects.push_back(new GameObject("floorplane", "GrassSamp1.jpg",
		GUVector4(0, 0, 0), GUAxisAngle(0, 0, 0, 0), GUVector4(100, 0.01, 100), &standardShader));

	// Create road segments
	int rSize = 5; // road size
	for (int i = 0; i < 50; ++i)
	{
		GameObject* road = new GameObject("roadsegment", "road.jpg",
			GUVector4(0, 0.01, 220  -i * (2 * rSize)), GUAxisAngle(0, 1, 0, PI / 2), GUVector4(rSize, 0.01, rSize), &standardShader);

		road->setLightBoost(10);
		gameObjects.push_back(road);
	}
	
	// Traffic cones
	for (int i = 0; i < 40; ++i)
	{
		float r = rand() % 10;
		r /= 100;

		gameObjects.push_back(new GameObject("traffic_cone", "cone_uv.PNG",
			GUVector4(r, 0.25, -4 - i * 3), GUAxisAngle(0, 1, 0, (1.0 / ((rand() % 4) + 1)) * PI), GUVector4(0.05, 0.07, 0.05), &standardShader));
	}

	// Power pilons
	for (int i = 0; i < 20; ++i)
	{
		GameObject* pilon = new GameObject("pilon", "old wood.jpg",
			GUVector4(-6, 0, -4 - i * 10), GUAxisAngle(0, 1, 0, PI / 2), GUVector4(0.5, 0.5, 0.5), &standardShader);
		pilon->setLightBoost(20);

		gameObjects.push_back(pilon);
	}

	// Cactii
	for (int i = 0; i < 20; ++i)
	{
		float r = rand() % 10;
		r /= 50;

		GameObject* cactus = new GameObject("cactus", "cactustexture.PNG",
			GUVector4(-6 + r, 0, -2 - i * 5 + r * 3), GUAxisAngle(0, 1, 0, (1.0 / ((rand() % 4) + 1)) * PI), GUVector4(0.5, 0.5, 0.5), &standardShader);

			cactus->setLightBoost(20);
			gameObjects.push_back(cactus);

		GameObject* cactus2 = new GameObject("cactus", "cactustexture.PNG",
			GUVector4(6 + r, 0, -2 - i * 5 + r * 3), GUAxisAngle(0, 1, 0, (1.0 / ((rand() % 4) + 1)) * PI), GUVector4(0.5, 0.5, 0.5), &standardShader);

		cactus2->setLightBoost(20);
		gameObjects.push_back(cactus2);
	}

	// Derelict shacks
	for (int i = 0; i < 20; ++i)
	{
		int r = rand() % 4;

		gameObjects.push_back(new GameObject("derelict_shack", "old wood.jpg",
			GUVector4(10 + r, 0.1, i * -4), GUAxisAngle(0, 1, 0, PI / r), GUVector4(0.01, 0.01, 0.01), &standardShader));

		gameObjects.push_back(new GameObject("derelict_shack", "old wood.jpg",
			GUVector4(-10 + r, 0.1, i * -4), GUAxisAngle(0, 1, 0, -PI / r), GUVector4(0.01, 0.01, 0.01), &standardShader));
	}

	if (!traffic)
		return;

	// Create Traffic!
	zHi = 200;
	zLo = -200;
	trafficPerLane = 20;
	span = abs(zLo - zHi);
	gap = span / trafficPerLane;
	cout << ">> >>" << gap;

	int firstTank = gameObjects.size(); // index of first tank


	string str[] = { "camo.png", "olive.png", "snow camo.jpg", "desert camo.jpg"};
	int str_len = 4;
	// Traffic
	for (int i = 0; i < trafficPerLane; ++i)
	{
		int oZ = rand() % 10; // offset Z
		int oX = oZ / 10; // offset X
		int skin = rand() % str_len; // Texture to use on tank

		Tank* npc = new Tank();

		GameObject* npcGo = new GameObject("Sherman", str[skin],
			GUVector4(1.5 + oX, 0, zLo + i * gap + oZ), GUAxisAngle(0, 1, 0, PI), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);

		GameObject* npcTurGo = new GameObject("ShermanTurret", str[skin],
			GUVector4(0, 0, 0), GUAxisAngle(0, 1, 0, PI), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);

		npcGo->setLightBoost(20);
		npcTurGo->setLightBoost(20);
		
		npc->init(npcGo, npcTurGo);
		npc->setCruiseControl(30);

		if (!i || i == (int)trafficPerLane / 2 ) 
		{
			npcGo->getAudioComponent()->addActionAudio("bgm", "music");
			npcGo->getAudioComponent()->playActionAudio("music");
			npcGo->getAudioComponent()->setActionAudioProperty("music", LOOP, 1);
			npcGo->getAudioComponent()->setActionAudioProperty("music", GAIN, 2);
			npcGo->drawAABB();

		}

		gameObjects.push_back(npcGo);
		gameObjects.push_back(npcTurGo);

		npcs.push_back(npc);
	}

	// Traffic
	for (int i = 0; i < trafficPerLane; ++i)
	{
		int oZ = rand() % 10;
		int oX = oZ / 10;
		int skin = rand() % str_len;

		Tank* npc = new Tank();

		GameObject* npcGo = new GameObject("Sherman", str[skin],
			GUVector4(3.5 + oX, 0, zLo + i * gap + oZ), GUAxisAngle(0, 1, 0, PI), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);

		GameObject* npcTurGo = new GameObject("ShermanTurret", str[skin],
			GUVector4(0, 0, 0), GUAxisAngle(0, 1, 0, PI), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);

		npcGo->setLightBoost(20);
		npcTurGo->setLightBoost(20);

		npc->init(npcGo, npcTurGo);
		npc->setCruiseControl(20);

		if (i == (int) trafficPerLane / 2)
		{
			npcGo->getAudioComponent()->addActionAudio("bgm", "music");
			npcGo->getAudioComponent()->playActionAudio("music");
			npcGo->getAudioComponent()->setActionAudioProperty("music", LOOP, 1);
			npcGo->getAudioComponent()->setActionAudioProperty("music", GAIN, 2);
			npcGo->drawAABB();
		}

		gameObjects.push_back(npcGo);
		gameObjects.push_back(npcTurGo);

		npcs.push_back(npc);
	}

	// More traffic
	for (int i = 0; i < trafficPerLane; ++i)
	{
		int oZ = rand() % 10;
		int oX = oZ / 10;
		int skin = rand() % str_len;

		Tank* npc = new Tank();

		GameObject* npcGo = new GameObject("Sherman", str[skin],
			GUVector4(-1.5 + oX, 0, zHi - i * gap + oZ), GUAxisAngle(0, 1, 0, 0), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);
		GameObject* npcTurGo = new GameObject("ShermanTurret", str[skin],
			GUVector4(0, 0, 0), GUAxisAngle(0, 1, 0, PI), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);

		npcGo->setLightBoost(20);
		npcTurGo->setLightBoost(20);

		npc->init(npcGo, npcTurGo);
		npc->setCruiseControl(40);

		if (!i || i == (int)trafficPerLane / 2) {
			npcGo->getAudioComponent()->addActionAudio("bgm", "music");
			npcGo->getAudioComponent()->playActionAudio("music");
			npcGo->getAudioComponent()->setActionAudioProperty("music", LOOP, 1);
			npcGo->getAudioComponent()->setActionAudioProperty("music", GAIN, 2);
			npcGo->drawAABB();
		}


		gameObjects.push_back(npcGo);
		gameObjects.push_back(npcTurGo);

		npcs.push_back(npc);
	}

	// Some more traffic
	for (int i = 0; i < trafficPerLane; ++i)
	{
		int oZ = rand() % 10;
		int oX = oZ / 10;
		int skin = rand() % str_len;

		Tank* npc = new Tank();

		GameObject* npcGo = new GameObject("Sherman", str[skin],
			GUVector4(-3.5 + oX, 0, zHi - i * gap + oZ), GUAxisAngle(0, 1, 0, 0), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);
		GameObject* npcTurGo = new GameObject("ShermanTurret", str[skin],
			GUVector4(0, 0, 0), GUAxisAngle(0, 1, 0, PI), GUVector4(shermanScale, shermanScale, shermanScale), &standardShader);

		npcGo->setLightBoost(20);
		npcTurGo->setLightBoost(20);

		npc->init(npcGo, npcTurGo);
		npc->setCruiseControl(20);

		if (i == (int) trafficPerLane/2 )
		{
			npcGo->getAudioComponent()->addActionAudio("bgm", "music");
			npcGo->getAudioComponent()->playActionAudio("music");
			npcGo->getAudioComponent()->setActionAudioProperty("music", LOOP, 1);
			npcGo->getAudioComponent()->setActionAudioProperty("music", GAIN, 2);
			npcGo->drawAABB();
		}


		gameObjects.push_back(npcGo);
		gameObjects.push_back(npcTurGo);

		npcs.push_back(npc);
	}

	

}

void TrafficDemo::updateTraffic()
{
	int dist = zHi + 5;
	
	for (int i = 0; i < npcs.size() / 2; ++i)
	{
		//npcs[i]->setAccel(0.5);
		npcs[i]->update(dT);

		GUVector4 pos = npcs[i]->getBodyGameObject()->getPosition();

		if (pos.z > dist)
		{
			pos.z = -dist;
			npcs[i]->getBodyGameObject()->setPosition(pos);
		}
	}

	for (int i = npcs.size() / 2; i < npcs.size(); ++i)
	{

		//npcs[i]->setAccel(1);
		npcs[i]->update(dT);

		GUVector4 pos = npcs[i]->getBodyGameObject()->getPosition();

		if (pos.z < -dist)
		{
			pos.z = dist;
			npcs[i]->getBodyGameObject()->setPosition(pos);
		}
	}
}

void TrafficDemo::adjustSunY(float input)
{
	float delta = input * dT;

	if (input < 0)
	{
		if (theSun.y + delta > -1)
			theSun.y += delta;
	}
	else
	{
		if (theSun.y + delta < 1)
			theSun.y += delta;
	}
}

void TrafficDemo::adjustSunOrbit(float input)
{
	theta += input * dT;

	float x = sin(theta);
	float z = cos(theta);
		
	theSun.x = x;
	theSun.z = z;

	float blue = z;
	if (blue < 0.2)
		blue = 0.2;
	lightCol.z = blue;
}


void TrafficDemo::update(void)
{
	Game::update();

	if (traffic)
		updateTraffic();
}







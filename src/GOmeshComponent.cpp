#include "GOmeshComponent.h"
#include "texture_loader.h"

GOmeshComponent::GOmeshComponent()
{
	meshCount = 0;
	meshArray = nullptr;

}

GOmeshComponent::~GOmeshComponent()
{
//	delete [] meshArray;
	//meshArray = nullptr;
	//meshName = "";
}


void GOmeshComponent::init(const aiScene* scene, string meshName, string texNameAndExt)
{
	this->meshName = meshName;
	this->texNameAndExt = texNameAndExt;

	string texturePath = "resources\\textures\\";
	texturePath += texNameAndExt;
	texture = fiLoadTexture(texturePath.c_str(), TextureProperties(false));
	
	meshCount = scene->mNumMeshes;

	meshArray = (MeshVBO**)malloc(meshCount * sizeof(MeshVBO*));

	for (int i = 0; i < meshCount; i++) {

		MeshVBO* mesh = new MeshVBO();
		mesh->init(scene, i);

		meshArray[i] = mesh;
	}	
}

GLuint* GOmeshComponent::getTexture()
{
	return &texture;
}

void GOmeshComponent::render()
{
	for (int i = 0; i < meshCount; ++i)
	{
		meshArray[i]->render();
	}

}

string GOmeshComponent::getName()
{
	return meshName;
}

string GOmeshComponent::getTextureName()
{
	return texNameAndExt;
}

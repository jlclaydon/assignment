#include "Game.h"
#include "IcebergDemo.h"
#include "TrafficDemo.h"
#include "LightColourDemo.h"
#include "MultiLightSourceDemo.h"
#include <iostream>

int main(int argc, char* argv[])
{
	// Constructors dont initialise anything
	LightColourDemo demo1 = LightColourDemo();
	MultiLightSourceDemo demo2 = MultiLightSourceDemo();
	IcebergDemo demo3 = IcebergDemo();
	TrafficDemo demo4 = TrafficDemo();


	int demo = 2;
	std::cout << "Run which demo?\n";
	std::cout << "1. Lighting Demo\n2. Multiple Light Source Demo\n3. Transparency demo\n4. Road only\n5. Traffic Demo\n";

	std::cin >> demo;
	
	switch (demo)
	{
	case 1:
		demo1.init();
		demo1.gameLoop();
		break;
	case 2:
		demo2.init();
		demo2.gameLoop();
	case 3:
		demo3.init();
		demo3.gameLoop();
		break;
	case 4:
		demo4.init(false);
		demo4.gameLoop();
		break;
	case 5:
		demo4.init(true);
		demo4.gameLoop();	
	}

	return 0;
}
#ifndef TANK_H_
#define TANK_H_

#include <CoreStructures\CoreStructures.h>
#include <iostream>
using std::cout; using std::endl;

using CoreStructures::GUAxisAngle;
using CoreStructures::GUVector4;
using CoreStructures::GUQuaternion;

class ActionAudio;
class GameObject;


class Tank
{
private:

	float dT; // delta time

	GameObject* body;
	GameObject* turret;

	float turretAngle;
		
	void turn(float angle);
	
	void gears(float signlessV);
	void turbo(float signlessV);
	void brakes();
	void tracks(float signlessV);
	void moveTank(float signlessV);
	void changeGear(int shift);

	/*** Driving mechs & audio vars **/
	// Uses the algorithms I created in the unity project for CIS5014. 
	// Modifies the volume and pitch the of engine and tank-tracks audio clips.
	// The vehicle approximates gears, and a turbo charger although power 
	// isnt calculated by torque + rpm, rather is calculated by the current gear
	// and the speed of the turbo charger. The aim was to exaggerate the turbo
	// as a mechanic, makes the tank feel heavy, slow to dodge
	ActionAudio* aaBrakes;
	ActionAudio* aaEngine;
	ActionAudio* aaTracks;
	ActionAudio* aaTurbo;
	
	float input_accel = 0;
	float input_turn = 0;
	float engineTorque = 10;
	float power = 0;
	float accelAhead = 0;
	float currentTurn = 0;
	float topSpeed = 55;
	float brakeForce = 5;
	float drag = 1;
	float turnVelocity = 0;
	const int turnVelocityMax = 1; 
	float forwardVelocity = 0;
	float turboSpeed = 0;
	float turboSpeedMax = 0; // variable
	int gear = 0;
	bool changingGear = false;
	bool changingUp = false;
	const float gearDelayMax = 1; 
	float gearDelay = 0; // timer
	bool brakesOn = false;
	float minSpeed = 0.1f;
	float cruiseControl = 0;
	float cruiseControlActive = false;

	float mapFunc(float x0, float x1, float y0, float y1, float v);

public:

	void setAccel(float input);
	void setTurn(float input);
	void setBrakes(bool brakesOn);

	void setCruiseControl(float speed);
	void cancelCruiseControl();

	void init(GameObject* tankBody, GameObject* tankTurret);


	void zeroVelocity();

	void rotateTurret(float input);

	void update(float dT);

	GUVector4* getForwardVector();
	GameObject* getBodyGameObject();

	float getForwardVelocity();

};

#endif
#include "AABB.h"
#include "shader_setup.h"

// declare an array of vertex coordinates
static float posArray[32] =
{
	0,0,0,1,	//	-x, -y, -z
	0,0,0,1,	//	 x, -y, -z
	0,0,0,1,	//	-x,	 y, -z
	0,0,0,1,	//	 x,	 y, -z
	0,0,0,1,	//  -x, -y, z
	0,0,0,1,	//   x, -y, z
	0,0,0,1,    //  -x,  y, z  
	0,0,0,1		//   x,  y, z
};

static float colArray[32] =
{
	1,0,1,1,
	1,0,1,1,
	1,0,1,1,
	1,0,1,1,
	1,0,1,1,
	1,0,1,1,
	1,0,1,1,
	1,0,1,1
};

static unsigned indexArray[] = 
{
	0,1,
	1,3,
	3,2,
	2,0,
	4,5,
	5,7,
	7,6,
	6,4,
	0,4,
	1,5,
	2,6,
	3,7
	
};

AABB::AABB(float length, float width, float height)
{
	
	// set X's
	for (int i = 0; i < 8; ++i)
	{
		float w = width;

		if (!(i % 2)) // if i is even, x is negative
			w *= -1;
		posArray[i * 4] = w;
	}	

	// set the Y's
	for (int i = 0; i < 8; ++i)
	{
		float h = height;

		if (i % 4 < 2) // if remainder of i/4 is 0 or 1, y is negative 
			h *= -1;

		posArray[i * 4 + 1] = h;
	}

	// and finally the Z's
	for (int i = 0; i < 8; ++i)
	{
		float l = length;

		if (i < 4) // if i is less than 4, z is negative
			l *= -1;

		posArray[i * 4 + 2] = l;
	}

	//printAabbCoords();

	// setup shaders, borrowing the shader from principle axis, since i dont need anything clever like lighting!
	shader = setupShaders(string("resources\\shaders\\basic_shader.vs"), string("resources\\shaders\\basic_shader.fs"));

	// setup VAO
	glGenVertexArrays(1, &vertexArrayObj);
	glBindVertexArray(vertexArrayObj);

	// setup position VBO
	glGenBuffers(1, &vertexBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(posArray), posArray, GL_STATIC_DRAW);

	// link to shader file, first argument is the attribute slot!, conferes to location number of the shaders 'in' location
	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);

	// setup vbo for colour attribute
	glGenBuffers(1, &colourBuffer);
	glBindBuffer(GL_ARRAY_BUFFER, colourBuffer);
	glBufferData(GL_ARRAY_BUFFER, sizeof(colArray), colArray, GL_STATIC_DRAW);

	// link to shader, location=1
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_TRUE, 0, (const GLvoid*)0);

	//setup index buffer
	glGenBuffers(1, &indexBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indexArray), indexArray, GL_STATIC_DRAW);

	// enable vertex buffers (positions + colours)
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);

	// unbind VAO
	glBindVertexArray(0);

}

AABB::AABB()
{
	
}

AABB::~AABB()
{
	glBindVertexArray(0);

	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glDeleteBuffers(1, &vertexBuffer);
	glDeleteBuffers(1, &colourBuffer);
	glDeleteBuffers(1, &indexBuffer);

	glDeleteVertexArrays(1, &vertexArrayObj);

	glDeleteShader(shader);
}

void AABB::render(const CoreStructures::GUMatrix4& T)
{
	// Pulled straight from the principle axis file

	static GLint mvpLocation = glGetUniformLocation(shader, "mvpMatrix");

	glUseProgram(shader);
	glUniformMatrix4fv(mvpLocation, 1, GL_FALSE, (const GLfloat*)&(T.M));

	glBindVertexArray(vertexArrayObj);

	glDrawElements(GL_LINES, 24, GL_UNSIGNED_INT, (const GLvoid*)0);

	glBindVertexArray(0);
}

void AABB::printAabbCoords()
{
	cout << "Printing AABB:\n";
	for (int i = 0; i < 8; ++i)
	{
		for (int j = 0; j < 4; ++j)
		{
			cout << posArray[(i * 4) + j] << ", ";
		}
		cout << endl;
	}
}
